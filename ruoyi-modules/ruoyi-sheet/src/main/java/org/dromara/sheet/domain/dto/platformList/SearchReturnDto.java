package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;

import java.util.List;

/**
 * 通用列表搜索及按钮返回DTO
 * @author yangshanlin
 * @date 2023/9/4 18:49
 * @describe
 */
@Data
public class SearchReturnDto {

    /**
     * 按钮配置
     */
    List<SearchButtonDto> buttonList;

    /**
     * 搜索配置
     */
    List<SearchConfigDto> searchConfigList;


}
