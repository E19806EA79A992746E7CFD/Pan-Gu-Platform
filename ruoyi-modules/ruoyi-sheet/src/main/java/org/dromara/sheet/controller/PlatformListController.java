package org.dromara.sheet.controller;

import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.web.core.BaseController;
import org.dromara.sheet.domain.dto.platformList.RequestParamsDto;
import org.dromara.sheet.domain.dto.platformList.SearchReturnDto;
import org.dromara.sheet.domain.dto.platformList.TableDataDto;
import org.dromara.sheet.service.PlatformListService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * 平台通用列表接口
 *
 * @author yangshanlin
 * @date 2023/8/31 15:10
 * @describe
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/platform/list")
public class PlatformListController extends BaseController {

    private final PlatformListService platformListService;


    /**
     * 获取列表搜索条件及按钮配置
     * @param sheetId
     * @return
     */
    @GetMapping("/getSearchAndButtonConfig")
    public R<SearchReturnDto> list(Long sheetId) {
        return R.ok(platformListService.getSearchAndButtonConfig(sheetId));
    }

    /**
     * 获取平台通用列表数据
     * @param paramsDto     请求参数
     * @return
     */
    @PostMapping("/getListData")
    public R<TableDataDto> getListData(@RequestBody RequestParamsDto paramsDto) {
        return R.ok(platformListService.getListData(paramsDto));
    }

    /**
     * 删除平台通用列表数据（逻辑删除）
     * @param ids
     * @param sheetId
     * @return
     */
    @DeleteMapping("/deleteByIds")
    @RepeatSubmit
    @Transactional
    public R<Void> deleteByIds(@RequestParam("ids") String ids, Long sheetId) {
        return toAjax(platformListService.deleteByIds(ids, sheetId));
    }
}
