package org.dromara.sheet.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.dromara.sheet.domain.dto.SysUserDto;
import org.dromara.sheet.domain.dto.platformList.DataOssDto;
import org.dromara.sheet.domain.dto.platformList.DelParamsDto;
import org.dromara.sheet.domain.dto.platformList.SearchParamsDto;

import java.util.List;
import java.util.Map;

/**
 * 平台列表mapper接口
 * @author Ysl
 */
public interface PlatformListMapper {

    /**
     * 获取平台通用列表数据
     * @param searchParams
     * @return
     */
    Page<Map<String, Object>> getDataList(@Param("page") Page<Map<String, Object>> page, @Param("params") SearchParamsDto searchParams);

    /**
     * 获取oss url
     * @param ossDtoList
     * @return
     */
    List<DataOssDto> getOSSUrl(List<DataOssDto> ossDtoList);

    /**
     * 获取系统用户
     * @return
     */
    List<SysUserDto> getUserList();

    /**
     * 删除平台通用列表数据
     * @param dto
     * @return
     */
    int deleteByIds(DelParamsDto dto);
}
