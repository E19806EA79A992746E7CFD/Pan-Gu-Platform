package org.dromara.sheet.domain.bo;

import io.github.linpeilie.annotations.AutoMapper;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.sheet.domain.SysSheetFormFieldAuth;

/**
 * 应用单字段授权业务对象 sys_sheet_form_field_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetFormFieldAuth.class, reverseConvertGenerate = false)
public class SysSheetFormFieldAuthBo extends BaseEntity {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 角色ID
     */
    @NotNull(message = "角色ID不能为空", groups = { EditGroup.class })
    private Long roleId;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 表单ID
     */
    @NotNull(message = "表单ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long formId;

    /**
     * 应用字段表ID
     */
    @NotNull(message = "应用字段表ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long fieldId;

    /**
     * 是否授予，使用字典
     */
    @NotBlank(message = "是否授予，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isAuth;

    /**
     * 是否隐藏，使用字典
     */
    @NotBlank(message = "是否隐藏，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isHidden;

    /**
     * 是否编辑，是否用字典
     */
    @NotBlank(message = "是否编辑，是否用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isEdit;


}
