package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;

import java.util.List;

/**
 * 通用列表搜索DTO
 * @author yangshanlin
 * @date 2023/9/4 16:53
 * @describe
 */
@Data
public class SearchConfigDto {

    /**
     * 主键ID
     */
    private Long sheetSearchId;


    /**
     * 搜索类型，使用字典（table_search_type）
     */
    private String searchType;

    /**
     * 搜索标题
     */
    private String searchName;

    /**
     * 搜索提示
     */
    private String searchTip;


    /**
     * 是否高级搜索，使用字典（sys_yes_no）
     */
    private String seniorSearch;

    /**
     * 数据源类型【1：数据字典】【2：物理表】
     */
    private Integer dataSourceType;

    /**
     * 数据源数据
     */
    private List<SearchDataSourceDto> dataSourceList;
}
