package org.dromara.sheet.mapper;

import org.dromara.sheet.domain.SysSheetField;
import org.dromara.sheet.domain.dto.platformList.TableHeadDto;
import org.dromara.sheet.domain.vo.SysSheetFieldVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

import java.util.List;

/**
 * 应用字段Mapper接口
 *
 * @author Ysl
 * @date 2023-07-26
 */
public interface SysSheetFieldMapper extends BaseMapperPlus<SysSheetField, SysSheetFieldVo> {

    /**
     * 获取平台列表配置显示的字段
     * @param sheetId
     * @return
     */
    List<TableHeadDto> getHeadList(Long sheetId);
}
