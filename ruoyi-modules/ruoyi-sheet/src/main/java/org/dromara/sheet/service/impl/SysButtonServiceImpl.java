package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.sheet.domain.SysButton;
import org.dromara.sheet.domain.bo.SysButtonBo;
import org.dromara.sheet.domain.vo.SysButtonVo;
import org.dromara.sheet.mapper.SysButtonMapper;
import org.dromara.sheet.service.ISysButtonService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 控件维护Service业务层处理
 *
 * @author Ysl
 * @date 2023-09-01
 */
@RequiredArgsConstructor
@Service
public class SysButtonServiceImpl implements ISysButtonService {

    private final SysButtonMapper baseMapper;

    /**
     * 查询控件维护
     */
    @Override
    public SysButtonVo queryById(Long buttonId){
        return baseMapper.selectVoById(buttonId);
    }

    /**
     * 查询控件维护列表
     */
    @Override
    public TableDataInfo<SysButtonVo> queryPageList(SysButtonBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysButton> lqw = buildQueryWrapper(bo);
        Page<SysButtonVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询控件维护列表
     */
    @Override
    public List<SysButtonVo> queryList(SysButtonBo bo) {
        LambdaQueryWrapper<SysButton> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysButton> buildQueryWrapper(SysButtonBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysButton> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysButton::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), SysButton::getCode, bo.getCode());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), SysButton::getType, bo.getType());
        lqw.eq(bo.getStyle() != null, SysButton::getStyle, bo.getStyle());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysButton::getStatus, bo.getStatus());
        lqw.eq(bo.getSort() != null, SysButton::getSort, bo.getSort());
        lqw.orderByAsc( SysButton::getSort);
        return lqw;
    }

    /**
     * 新增控件维护
     */
    @Override
    public Boolean insertByBo(SysButtonBo bo) {
        SysButton add = MapstructUtils.convert(bo, SysButton.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setButtonId(add.getButtonId());
        }
        return flag;
    }

    /**
     * 修改控件维护
     */
    @Override
    public Boolean updateByBo(SysButtonBo bo) {
        SysButton update = MapstructUtils.convert(bo, SysButton.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysButton entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除控件维护
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
