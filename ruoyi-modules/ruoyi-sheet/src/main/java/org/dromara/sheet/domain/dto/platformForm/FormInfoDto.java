package org.dromara.sheet.domain.dto.platformForm;

import lombok.Data;
import org.dromara.sheet.domain.dto.platformList.SearchButtonDto;
import org.dromara.sheet.domain.vo.SysSheetVo;

import java.util.List;

/**
 * 表单信息DTO
 *
 * @author yangshanlin
 * @date 2023/9/19 17:31
 * @describe
 */
@Data
public class FormInfoDto {

    /**
     * 表单ID
     */
    private Long formId;

    /**
     * 表单名称
     */
    private String formName;

    /**
     * 显示方式【1：表单】【2：列表】
     */
    private Integer showType;

    /**
     * 是否子表单【1：是】【0：否】
     */
    private Integer isChild;

    /**
     * 表单字段分组
     */
    private List<FormInfoGroupDto> groupList;

    /**
     * 按钮配置
     */
    List<SearchButtonDto> buttonList;

    /**
     * 表单绑定的应用
     */
    SysSheetVo sheet;
}
