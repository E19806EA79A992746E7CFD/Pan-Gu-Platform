package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.sheet.service.ISysSheetSourceService;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.sheet.domain.vo.SysSheetSourceVo;
import org.dromara.sheet.domain.bo.SysSheetSourceBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 应用数据源
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/sheetSource")
public class SysSheetSourceController extends BaseController {

    private final ISysSheetSourceService sysSheetSourceService;

    /**
     * 查询应用数据源列表
     */
    @SaCheckPermission("system:sheetSource:list")
    @GetMapping("/list")
    public TableDataInfo<SysSheetSourceVo> list(SysSheetSourceBo bo, PageQuery pageQuery) {
        return sysSheetSourceService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出应用数据源列表
     */
    @SaCheckPermission("system:sheetSource:export")
    @Log(title = "应用数据源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysSheetSourceBo bo, HttpServletResponse response) {
        List<SysSheetSourceVo> list = sysSheetSourceService.queryList(bo);
        ExcelUtil.exportExcel(list, "应用数据源", SysSheetSourceVo.class, response);
    }

    /**
     * 获取应用数据源详细信息
     *
     * @param sheetSourceId 主键
     */
    @SaCheckPermission("system:sheetSource:query")
    @GetMapping("/{sheetSourceId}")
    public R<SysSheetSourceVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long sheetSourceId) {
        return R.ok(sysSheetSourceService.queryById(sheetSourceId));
    }

    /**
     * 新增应用数据源
     */
    @SaCheckPermission("system:sheetSource:add")
    @Log(title = "应用数据源", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysSheetSourceBo bo) {
        return toAjax(sysSheetSourceService.insertByBo(bo));
    }

    /**
     * 修改应用数据源
     */
    @SaCheckPermission("system:sheetSource:edit")
    @Log(title = "应用数据源", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysSheetSourceBo bo) {
        return toAjax(sysSheetSourceService.updateByBo(bo));
    }

    /**
     * 删除应用数据源
     *
     * @param sheetSourceIds 主键串
     */
    @SaCheckPermission("system:sheetSource:remove")
    @Log(title = "应用数据源", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sheetSourceIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] sheetSourceIds) {
        return toAjax(sysSheetSourceService.deleteWithValidByIds(List.of(sheetSourceIds), true));
    }

    /**
     * 获取应用绑定的数据源
     * @param sheetId
     * @return
     */
    @GetMapping("/listDataSourceBySheetId/{sheetId}")
    public R<List<SysSheetSourceVo>> listDataSourceBySheetId(@PathVariable Long sheetId) {
        return R.ok(sysSheetSourceService.listDataSourceBySheetId(sheetId));
    }
}
