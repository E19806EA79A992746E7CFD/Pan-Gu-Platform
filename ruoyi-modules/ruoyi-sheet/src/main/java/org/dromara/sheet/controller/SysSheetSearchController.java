package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.bo.SysSheetSearchBo;
import org.dromara.sheet.domain.vo.SysSheetSearchVo;
import org.dromara.sheet.service.ISysSheetSearchService;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;

/**
 * 应用搜索
 *
 * @author Ysl
 * @date 2023-08-31
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/sheetSearch")
public class SysSheetSearchController extends BaseController {

    private final ISysSheetSearchService sysSheetSearchService;

    /**
     * 查询应用搜索列表
     */
    @SaCheckPermission("system:sheetSearch:list")
    @GetMapping("/list")
    public TableDataInfo<SysSheetSearchVo> list(SysSheetSearchBo bo, PageQuery pageQuery) {
        return sysSheetSearchService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出应用搜索列表
     */
    @SaCheckPermission("system:sheetSearch:export")
    @Log(title = "应用搜索", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysSheetSearchBo bo, HttpServletResponse response) {
        List<SysSheetSearchVo> list = sysSheetSearchService.queryList(bo);
        ExcelUtil.exportExcel(list, "应用搜索", SysSheetSearchVo.class, response);
    }

    /**
     * 获取应用搜索详细信息
     *
     * @param sheetSearchId 主键
     */
    @SaCheckPermission("system:sheetSearch:query")
    @GetMapping("/{sheetSearchId}")
    public R<SysSheetSearchVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long sheetSearchId) {
        return R.ok(sysSheetSearchService.queryById(sheetSearchId));
    }

    /**
     * 新增应用搜索
     */
    @SaCheckPermission("system:sheetSearch:add")
    @Log(title = "应用搜索", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysSheetSearchBo bo) {
        return toAjax(sysSheetSearchService.insertByBo(bo));
    }

    /**
     * 修改应用搜索
     */
    @SaCheckPermission("system:sheetSearch:edit")
    @Log(title = "应用搜索", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysSheetSearchBo bo) {
        return toAjax(sysSheetSearchService.updateByBo(bo));
    }

    /**
     * 删除应用搜索
     *
     * @param sheetSearchIds 主键串
     */
    @SaCheckPermission("system:sheetSearch:remove")
    @Log(title = "应用搜索", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sheetSearchIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] sheetSearchIds) {
        return toAjax(sysSheetSearchService.deleteWithValidByIds(List.of(sheetSearchIds), true));
    }
}
