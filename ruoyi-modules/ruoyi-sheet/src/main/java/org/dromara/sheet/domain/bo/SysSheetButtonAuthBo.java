package org.dromara.sheet.domain.bo;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysSheetButtonAuth;

/**
 * 应用按钮授权业务对象 sys_sheet_button_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetButtonAuth.class, reverseConvertGenerate = false)
public class SysSheetButtonAuthBo extends BaseEntity {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 角色ID
     */
    @NotNull(message = "角色ID不能为空", groups = { EditGroup.class })
    private Long roleId;

    /**
     * 绑定ID，应用ID或者表单ID
     */
    @NotNull(message = "绑定ID，应用ID或者表单ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long bindId;

    /**
     * 按钮ID
     */
    @NotNull(message = "按钮ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sheetButtonId;

    /**
     * 类型，使用字典（列表、表单）
     */
    @NotNull(message = "类型，使用字典（列表、表单）不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer type;


}
