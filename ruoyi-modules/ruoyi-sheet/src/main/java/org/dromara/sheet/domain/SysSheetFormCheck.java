package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 单校验对象 sys_sheet_form_check
 *
 * @author Ysl
 * @date 2023-09-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet_form_check")
public class SysSheetFormCheck extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "sheet_form_check_id")
    private Long sheetFormCheckId;

    /**
     * 表单ID
     */
    private Long formId;

    /**
     * 表字段ID
     */
    private Long sheetFieldId;

    /**
     * 字段中文名称
     */
    private String sheetFieldName;

    /**
     * 字段名称
     */
    private String sheetField;

    /**
     * 校验类型，使用字典，1.必填，2.长度，3.正则，4接口
     */
    private String checkType;

    /**
     * 错误提示
     */
    private String errorMsg;

    /**
     * 最小长度
     */
    private Long minLength;

    /**
     * 最大长度
     */
    private Long maxLength;

    /**
     * 正则表达式
     */
    private String regular;

    /**
     * 校验接口模板ID
     */
    private Long apiTemplateId;

    /**
     * 校验接口名称
     */
    private String apiTemplateName;

    /**
     * 校验接口地址
     */
    private String apiTemplateUrl;


}
