package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysSheetForm;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 表单管理视图对象 sys_sheet_form
 *
 * @author lhk
 * @date 2023-09-04
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetForm.class)
public class SysSheetFormVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long sheetFormId;

    /**
     * 表单名称
     */
    private String path;

    /**
     * 父级ID
     */
    @ExcelProperty(value = "父级ID")
    private Long pid;

    /**
     * 表单名称
     */
    @ExcelProperty(value = "表单名称")
    private String name;

    /**
     * 显示名称
     */
    private String showName;

    /**
     * 页签
     */
    @ExcelProperty(value = "页签")
    private String tabName;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private Long sheetId;

    /**
     * 应用名称
     */
    @ExcelProperty(value = "应用名称")
    private String sheetName;

    /**
     * 排序号
     */
    @ExcelProperty(value = "排序号")
    private Long sort;

    /**
     * 显示方式
     */
    @ExcelProperty(value = "显示方式")
    private Integer showType;

    /**
     * 是否启用
     */
    @ExcelProperty(value = "是否启用")
    private String status;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


    /**
     * 是否授权
     */
    private String isAuth;
}
