package org.dromara.sheet.domain.bo;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysSheetSearch;

/**
 * 应用搜索业务对象 sys_sheet_search
 *
 * @author Ysl
 * @date 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetSearch.class, reverseConvertGenerate = false)
public class SysSheetSearchBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotNull(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long sheetSearchId;

    /**
     *  应用ID
     */
    @NotNull(message = " 应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sheetId;

    /**
     * 搜索类型，使用字典

     */
    private String searchType;

    /**
     * 搜索标题
     */
    @NotBlank(message = "搜索标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String searchName;

    /**
     * 判断逻辑，使用字典
     */
    private String checkType;

    /**
     * 搜索字段ID，多个逗号隔开
     */
    private String searchFieldId;

    /**
     * 搜索字段，多个逗号隔开
     */
    @NotBlank(message = "搜索字段不能为空", groups = { AddGroup.class, EditGroup.class })
    private String searchFieldName;

    /**
     * 是否启用，使用字典
     */
    private String status;

    /**
     * 是否高级搜索，使用字典
     */
    private String seniorSearch;

    /**
     * 数据源类型【1：数据字典】【2：物理表】
     */
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    private String dataSourceName;

    /**
     * 是否主应用
     */
    private String isMaster;

    /**
     * 子应用名称
     */
    private Long childSheetId;

    /**
     * 子应用主键字段
     */
    private String childSheetKeyId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 搜索提示
     */
    private String searchTip;
}
