package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 应用控件对象 sys_provide_service
 *
 * @author Ysl
 * @date 2023-11-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_provide_service")
public class SysProvideService extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "service_id")
    private Long serviceId;

    /**
     * 服务名称
     */
    private String name;

    /**
     * 服务编码
     */
    private String code;

    /**
     * 接口域名
     */
    private String apiDomain;

    /**
     * 是否启用，使用字典
     */
    private String status;

    /**
     * 排序号
     */
    private Long sort;

    /**
     * 备注
     */
    private String remark;


}
