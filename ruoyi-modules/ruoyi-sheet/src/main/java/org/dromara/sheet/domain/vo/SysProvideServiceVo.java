package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysProvideService;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用控件视图对象 sys_provide_service
 *
 * @author Ysl
 * @date 2023-11-13
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysProvideService.class)
public class SysProvideServiceVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long serviceId;

    /**
     * 服务名称
     */
    @ExcelProperty(value = "服务名称")
    private String name;

    /**
     * 服务编码
     */
    @ExcelProperty(value = "服务编码")
    private String code;

    /**
     * 接口域名
     */
    @ExcelProperty(value = "接口域名")
    private String apiDomain;

    /**
     * 是否启用，使用字典
     */
    @ExcelProperty(value = "是否启用，使用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String status;

    /**
     * 排序号
     */
    @ExcelProperty(value = "排序号")
    private Long sort;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
