package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetSearch;
import org.dromara.sheet.domain.dto.platformList.SearchDataSourceDto;
import org.dromara.sheet.domain.vo.SysDataSourceVo;
import org.dromara.sheet.domain.vo.SysSheetSearchVo;

import java.util.List;

/**
 * 应用搜索Mapper接口
 *
 * @author Ysl
 * @date 2023-08-31
 */
public interface SysSheetSearchMapper extends BaseMapperPlus<SysSheetSearch, SysSheetSearchVo> {

    /**
     * 获取通用列表搜索字段自定义数据源
     * @param dataSource
     * @return
     */
    List<SearchDataSourceDto> getDiyDataSourceList(SysDataSourceVo dataSource);

    /**
     * 获取通用列表搜索字段字典数据源
     * @param dataSourceId
     * @return
     */
    List<SearchDataSourceDto> getDictDataList(Long dataSourceId);
}
