package org.dromara.sheet.domain.dto.platformList;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.List;

/**
 * 通用搜索按钮DTO
 * @author yangshanlin
 * @date 2023/9/4 11:46
 * @describe
 */
@Data
public class SearchButtonDto {

    /**
     * 主键ID
     */
    private Long sheetButtonId;


    /**
     * 控件编码
     */
    private String buttonCode;

    /**
     * 按钮类型
     */
    private String buttonType;

    /**
     * 按钮图标
     */
    private String buttonIcon;

    /**
     * 显示名称
     */
    @ExcelProperty(value = "显示名称")
    private String showName;

    /**
     * 显示方式，使用字典（form_show_type）
     */
    private Integer showType;

    /**
     * 打开方式，使用字典（sys_button_type）
     */
    private Integer openType;

    /**
     * 显示位置，使用字典（button_show_area）
     */
    private Integer showArea;

    /**
     * 接口地址
     */
    private String apiUrl;

    /**
     * 提示
     */
    @ExcelProperty(value = "提示")
    private String showTip;

    /**
     * 是否有分组【1：是】【0：否】
     */
    private Integer type;

    /**
     * 分组
     */
    private List<SearchButtonDto> groupList;

}
