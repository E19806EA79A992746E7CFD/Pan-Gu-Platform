package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.sheet.domain.SysProvideService;
import org.dromara.sheet.domain.bo.SysProvideServiceBo;
import org.dromara.sheet.domain.vo.SysProvideServiceVo;
import org.dromara.sheet.mapper.SysProvideServiceMapper;
import org.dromara.sheet.service.ISysProvideServiceService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 应用控件Service业务层处理
 *
 * @author Ysl
 * @date 2023-11-13
 */
@RequiredArgsConstructor
@Service
public class SysProvideServiceServiceImpl implements ISysProvideServiceService {

    private final SysProvideServiceMapper baseMapper;

    /**
     * 查询应用控件
     */
    @Override
    public SysProvideServiceVo queryById(Long serviceId){
        return baseMapper.selectVoById(serviceId);
    }

    /**
     * 查询应用控件列表
     */
    @Override
    public TableDataInfo<SysProvideServiceVo> queryPageList(SysProvideServiceBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysProvideService> lqw = buildQueryWrapper(bo);
        Page<SysProvideServiceVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询应用控件列表
     */
    @Override
    public List<SysProvideServiceVo> queryList(SysProvideServiceBo bo) {
        LambdaQueryWrapper<SysProvideService> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysProvideService> buildQueryWrapper(SysProvideServiceBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysProvideService> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysProvideService::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), SysProvideService::getCode, bo.getCode());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysProvideService::getStatus, bo.getStatus());
        return lqw;
    }

    /**
     * 新增应用控件
     */
    @Override
    public Boolean insertByBo(SysProvideServiceBo bo) {
        SysProvideService add = MapstructUtils.convert(bo, SysProvideService.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setServiceId(add.getServiceId());
        }
        return flag;
    }

    /**
     * 修改应用控件
     */
    @Override
    public Boolean updateByBo(SysProvideServiceBo bo) {
        SysProvideService update = MapstructUtils.convert(bo, SysProvideService.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysProvideService entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除应用控件
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
