package org.dromara.sheet.mapper;

import org.dromara.sheet.domain.dto.OssDto;

import java.util.List;
import java.util.Map;

/**
 * 平台表单mapper接口
 * @author Ysl
 */
public interface PlatformFormMapper {

    /**
     * 保存表格数据
     * @param params
     * @return
     */
    int saveTableData(Map<String,Object> params);

    /**
     * 更新表格数据
     * @param columnMap2
     * @return
     */
    int updateTableData(Map<String, Object> columnMap2);

    /**
     * 获取表格数据
     * @param params
     * @return
     */
    List<Map<String, Object>> getTableDataMap(Map<String, Object> params);

    /**
     * 获取oss文件信息
     * @param ossIdList
     * @return
     */
    List<OssDto> getOssList(List<Long> ossIdList);

    /**
     * 删除表单数据（逻辑删除）
     * @param params
     * @return
     */
    int delTableData(Map<String, Object> params);
}
