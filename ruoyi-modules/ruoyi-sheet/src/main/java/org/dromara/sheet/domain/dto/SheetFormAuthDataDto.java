package org.dromara.sheet.domain.dto;

import lombok.Data;
import org.dromara.sheet.domain.vo.SysSheetButtonAuthVo;
import org.dromara.sheet.domain.vo.SysSheetFormAuthVo;
import org.dromara.sheet.domain.vo.SysSheetFormFieldAuthVo;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 应用单授权数据
 * @author Ysl
 */
@Data
public class SheetFormAuthDataDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 应用表单授权列表
     */
    private List<SysSheetFormAuthVo> sheetFormAuthList;

    /**
     * 应用字段授权列表
     */
    private List<SysSheetFormFieldAuthVo> sheetFieldAuthList;

    /**
     * 表单按钮授权列表
     */
    private List<SysSheetButtonAuthVo> sheetButtonAuthList;
}
