package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;
import org.dromara.common.core.utils.StringUtils;

import java.util.List;

/**
 * 通用列表表头DTO
 * @author yangshanlin
 * @date 2023/9/4 23:44
 * @describe
 */
@Data
public class TableHeadDto {

    /**
     * 显示名称
     */
    private String headName;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 数据源ID
     */
    private Long dataSourceId;

    /**
     * 数据源类型
     */
    private Integer dataSourceType;

    /**
     * 列表排序
     */
    private Integer tableSort;

    /**
     * 列表字段宽度
     */
    private Integer tableWidth;

    /**
     * 列表是否显示，使用字典（sys_yes_no）
     */
    private String showTable;

    /**
     * 展示方式
     */
    private Integer showType;

    /**
     * 字典数据源数据
     */
    private List<SearchDataSourceDto> sourceList;

    /**
     * 驼峰式命名法
     * @param fieldName 字段名称
     */
    public void setFieldName(String fieldName) {
        this.fieldName = StringUtils.toCamelCase(fieldName);
    }
}
