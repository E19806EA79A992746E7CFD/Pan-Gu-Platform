package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysSheetFormAuth;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用单授权视图对象 sys_sheet_form_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetFormAuth.class)
public class SysSheetFormAuthVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;


    /**
     * 角色ID
     */
    @ExcelProperty(value = "角色ID")
    private Long roleId;

    /**
     * 表单ID
     */
    @ExcelProperty(value = "表单ID")
    private Long formId;

}
