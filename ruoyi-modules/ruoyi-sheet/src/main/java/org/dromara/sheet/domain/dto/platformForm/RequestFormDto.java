package org.dromara.sheet.domain.dto.platformForm;

import lombok.Data;
import org.dromara.sheet.domain.vo.SysSheetSelectVo;

import java.util.List;
import java.util.Map;

/**
 * 表单请求参数DTO
 * @author yangshanlin
 * @date 2023/10/26 16:18
 * @describe
 */
@Data
public class RequestFormDto {

    /**
     * 主表单数据
     */
    private List<Map<String, Object>> masterDataList;

    /**
     * 子表单数据
     */
    private List<Map<String, Object>> childDataList;

    /**
     * 子表单删除数据
     */
    private List<Map<String, Object>> childDelList;

    /**
     * 下拉选项数据
     */
    private List<SysSheetSelectVo> selectDataList;

    /**
     * 应用ID
     */
    private Long sheetId;
}
