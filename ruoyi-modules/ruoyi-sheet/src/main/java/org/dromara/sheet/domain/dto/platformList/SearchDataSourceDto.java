package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 通用数据源DTO
 * @author yangshanlin
 * @date 2023/9/4 17:28
 * @describe
 */
@Data
public class SearchDataSourceDto {

    /**
     * 下拉ID
     */
    private String value;

    /**
     * 父级ID
     */
    private String parentId;

    /**
     * 路径（用于树状）
     */
    private String path;

    /**
     * 下拉值
     */
    private String label;

    /**
     * 按钮类型
     */
    private String elTagType;

    /**
     * 按钮样式
     */
    private String elTagClass;

    /**
     * 子集
     */
    private List<SearchDataSourceDto> children;


    public List<SearchDataSourceDto> builTree(List<SearchDataSourceDto> dataList){
        List<SearchDataSourceDto> treeMenus = new ArrayList<>();
        for(SearchDataSourceDto menuNode : getRootNode(dataList)) {
            menuNode=buildChilTree(menuNode,dataList);
            treeMenus.add(menuNode);
        }
        return treeMenus;
    }

    public List<SearchDataSourceDto> getRootNode(List<SearchDataSourceDto> dataList) {
        List<SearchDataSourceDto> rootMenuLists = new ArrayList<>();
        for(SearchDataSourceDto menuNode : dataList) {
            if(menuNode.getParentId().equals("0")) {
                rootMenuLists.add(menuNode);
            }
        }
        return rootMenuLists;
    }

    public SearchDataSourceDto buildChilTree(SearchDataSourceDto pNode, List<SearchDataSourceDto> dataList){
        List<SearchDataSourceDto> chilMenus = new ArrayList<>();
        for(SearchDataSourceDto menuNode : dataList) {
            if(menuNode.getParentId().equals(pNode.getValue())) {
                chilMenus.add(buildChilTree(menuNode, dataList));
            }
        }
        pNode.setChildren(chilMenus);
        return pNode;
    }

}
