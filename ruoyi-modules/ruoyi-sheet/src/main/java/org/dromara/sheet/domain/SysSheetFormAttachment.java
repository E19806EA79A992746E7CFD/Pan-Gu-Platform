package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 单附件对象 sys_sheet_form_attachment
 *
 * @author Ysl
 * @date 2023-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet_form_attachment")
public class SysSheetFormAttachment extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "sheet_form_attachment_id")
    private Long sheetFormAttachmentId;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 单据ID
     */
    private Long orderId;

    /**
     * 字段ID
     */
    private Long fieldId;

    /**
     * OSSID
     */
    private Long ossId;

    /**
     * url地址
     */
    private String url;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件后缀
     */
    private String fileSuffix;

    /**
     * 文件原名
     */
    private String originalName;

}
