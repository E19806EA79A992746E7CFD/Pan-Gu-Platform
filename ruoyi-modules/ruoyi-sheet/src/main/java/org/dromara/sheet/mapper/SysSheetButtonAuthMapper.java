package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetButtonAuth;
import org.dromara.sheet.domain.vo.SysSheetButtonAuthVo;

/**
 * 应用按钮授权Mapper接口
 *
 * @author Ysl
 * @date 2023-12-04
 */
public interface SysSheetButtonAuthMapper extends BaseMapperPlus<SysSheetButtonAuth, SysSheetButtonAuthVo> {

}
