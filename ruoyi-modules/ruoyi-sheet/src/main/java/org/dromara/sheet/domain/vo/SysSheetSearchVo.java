package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.common.translation.annotation.Translation;
import org.dromara.common.translation.constant.TransConstant;
import org.dromara.sheet.domain.SysSheetSearch;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用搜索视图对象 sys_sheet_search
 *
 * @author Ysl
 * @date 2023-08-31
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetSearch.class)
public class SysSheetSearchVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long sheetSearchId;

    /**
     *  应用ID
     */
    @ExcelProperty(value = " 应用ID")
    private Long sheetId;

    /**
     * 搜索类型，使用字典

     */
    @ExcelProperty(value = "搜索类型，使用字典")
    private String searchType;

    /**
     * 搜索标题
     */
    @ExcelProperty(value = "搜索标题")
    private String searchName;

    /**
     * 判断逻辑，使用字典
     */
    @ExcelProperty(value = "判断逻辑，使用字典")
    private String checkType;

    /**
     * 搜索字段ID，多个逗号隔开
     */
    @ExcelProperty(value = "搜索字段ID，多个逗号隔开")
    private String searchFieldId;

    /**
     * 搜索字段，多个逗号隔开
     */
    @ExcelProperty(value = "搜索字段，多个逗号隔开")
    private String searchFieldName;

    /**
     * 是否启用，使用字典
     */
    @ExcelProperty(value = "是否启用，使用字典")
    private String status;

    /**
     * 是否高级搜索，使用字典
     */
    @ExcelProperty(value = "是否高级搜索，使用字典")
    private String seniorSearch;

    /**
     * 数据源类型【1：数据字典】【2：物理表】
     */
    @ExcelProperty(value = "数据源类型【1：数据字典】【2：物理表】")
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    @ExcelProperty(value = "数据源ID")
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    @ExcelProperty(value = "数据源名称")
    private String dataSourceName;

    /**
     * 是否主应用
     */
    @ExcelProperty(value = "是否主应用")
    private String isMaster;

    /**
     * 子应用名称
     */
    @ExcelProperty(value = "子应用名称")
    @Translation(type = TransConstant.SHEET_ID_TO_NAME)
    private Long childSheetId;

    /**
     * 子应用主键字段
     */
    @ExcelProperty(value = "子应用主键字段")
    private String childSheetKeyId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 搜索提示
     */
    private String searchTip;

}
