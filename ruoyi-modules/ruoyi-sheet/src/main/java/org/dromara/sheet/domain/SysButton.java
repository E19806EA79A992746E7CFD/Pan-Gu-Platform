package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 控件维护对象 sys_button
 *
 * @author Ysl
 * @date 2023-09-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_button")
public class SysButton extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "button_id")
    private Long buttonId;

    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 按钮类型，使用字典
     */
    private String type;

    /**
     * 样式，使用字典
     */
    private String style;

    /**
     * 是否启用，使用字典
     */
    private String status;

    /**
     * 排序号
     */
    private Long sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 服务主键ID
     */
    private Long serviceId;

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 服务域名
     */
    private String apiDomain;

    /**
     * 服务接口地址
     */
    private String apiUrl;
}
