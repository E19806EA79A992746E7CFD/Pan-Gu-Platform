package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetSelect;
import org.dromara.sheet.domain.vo.SysSheetSelectVo;

/**
 * 应用选择关联Mapper接口
 *
 * @author Ysl
 * @date 2023-09-10
 */
public interface SysSheetSelectMapper extends BaseMapperPlus<SysSheetSelect, SysSheetSelectVo> {

}
