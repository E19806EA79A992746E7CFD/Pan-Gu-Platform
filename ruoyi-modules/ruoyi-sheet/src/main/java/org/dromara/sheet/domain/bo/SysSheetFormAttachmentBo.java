package org.dromara.sheet.domain.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysSheetFormAttachment;

/**
 * 单附件业务对象 sys_sheet_form_attachment
 *
 * @author Ysl
 * @date 2023-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetFormAttachment.class, reverseConvertGenerate = false)
public class SysSheetFormAttachmentBo extends BaseEntity {

    /**
     * 主键ID
     */
    private Long sheetFormAttachmentId;

    /**
     * 应用ID
     */
    @NotNull(message = "应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sheetId;

    /**
     * 单据ID
     */
    @NotNull(message = "单据ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long orderId;

    /**
     * 字段ID
     */
    private Long fieldId;

    /**
     * OSSID
     */
    @NotNull(message = "OSSID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long ossId;

    /**
     * url地址
     */
    @NotBlank(message = "url地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String url;

    /**
     * 文件名称
     */
    @NotBlank(message = "文件名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String fileName;

    /**
     * 文件后缀
     */
    private String fileSuffix;

    /**
     * 文件原名
     */
    private String originalName;

}
