package org.dromara.sheet.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * 列表下拉搜索枚举
 * @author yangshanlin
 * @date 2023/11/7 10:48
 * @describe
 */
@Getter
@AllArgsConstructor
public enum SearchTypeEnum {

    /**
     * 输入框
     */
    INPUT("1"),

    /**
     * 普通单选
     */
    SINGLE("2"),
    /**
     * 普通多选
     */
    MULTIPLE("3"),
    /**
     * 树子节点单选
     */
    TREE_SINGLE("4"),
    /**
     * 树子节点多选
     */
    TREE_MULTIPLE("5"),
    /**
     * 树任意单选
     */
    TREE_WILL_SINGLE("6"),
    /**
     * 树任意多选
     */
    TREE_WILL_MULTIPLE("7"),

    /**
     * 时间
     */
    DATE("8"),

    /**
     * 时间范围
     */
    BETWEEN_DATE("9");


    private final String searchType;

    /**
     * 获取下拉类型
     * @return
     */
    public static List<String> getSelectType() {
        return List.of(SINGLE.searchType, MULTIPLE.searchType, TREE_SINGLE.searchType, TREE_MULTIPLE.searchType, TREE_WILL_SINGLE.searchType, TREE_WILL_MULTIPLE.searchType);
    }

    /**
     * 获取树下拉类型
     * @return
     */
    public static List<String> getTreeSelectType() {
        return List.of(TREE_SINGLE.searchType, TREE_MULTIPLE.searchType, TREE_WILL_SINGLE.searchType, TREE_WILL_MULTIPLE.searchType);
    }

}
