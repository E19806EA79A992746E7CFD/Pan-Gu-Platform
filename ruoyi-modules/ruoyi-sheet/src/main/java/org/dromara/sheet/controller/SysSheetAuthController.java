package org.dromara.sheet.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.web.core.BaseController;
import org.dromara.sheet.domain.bo.SysSheetButtonAuthBo;
import org.dromara.sheet.domain.dto.SheetFormAuthDataDto;
import org.dromara.sheet.domain.vo.SheetAuthTreeSelectVo;
import org.dromara.sheet.service.SysSheetAuthService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 应用授权
 * @author yangshanlin
 * @date 2023/12/7 15:46
 * @describe
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/sheetAuth")
public class SysSheetAuthController extends BaseController {

    private final SysSheetAuthService sheetAuthService;

    /**
     * 获取应用按钮授权
     * @param roleId    角色ID
     * @return
     */
    @Log(title = "获取应用按钮授权", businessType = BusinessType.INSERT)
    @GetMapping("/getSheetButtonList")
    public R<SheetAuthTreeSelectVo> getSheetButtonList(Long roleId){
        return R.ok(sheetAuthService.getSheetButtonList(roleId));
    }

    /**
     * 保存应用授权
     */
    @SaCheckPermission("system:role:sheetAuth")
    @Log(title = "应用授权", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping("/saveSheetAuth")
    public R<Void> saveSheetAuth(@RequestBody List<SysSheetButtonAuthBo> buttonAuthBoList) {
        return toAjax(sheetAuthService.saveSheetAuth(buttonAuthBoList));
    }

    /**
     * 获取应用表单及按钮授权数据
     * @param roleId
     * @return
     */
    @RepeatSubmit()
    @GetMapping("/getSheetFormAuthData")
    public R<SheetFormAuthDataDto> getSheetFormAuthData(Long roleId) {
        return R.ok(sheetAuthService.getSheetFormAuthData(roleId));
    }

    /**
     * 保存应用表单授权
     * @param authData
     * @return
     */
    @SaCheckPermission("system:role:sheetFormAuth")
    @Log(title = "应用单授权", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PostMapping("/saveSheetFormAuthData")
    public R<Void> saveSheetFormAuthData(@RequestBody SheetFormAuthDataDto authData) {
        return toAjax(sheetAuthService.saveSheetFormAuthData(authData));
    }

}
