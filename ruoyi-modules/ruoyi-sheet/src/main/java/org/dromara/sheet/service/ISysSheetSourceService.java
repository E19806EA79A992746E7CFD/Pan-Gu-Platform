package org.dromara.sheet.service;

import org.dromara.sheet.domain.SysSheetSource;
import org.dromara.sheet.domain.vo.SysSheetSourceVo;
import org.dromara.sheet.domain.bo.SysSheetSourceBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 应用数据源Service接口
 *
 * @author Ysl
 * @date 2023-07-26
 */
public interface ISysSheetSourceService {

    /**
     * 查询应用数据源
     */
    SysSheetSourceVo queryById(Long sheetSourceId);

    /**
     * 查询应用数据源列表
     */
    TableDataInfo<SysSheetSourceVo> queryPageList(SysSheetSourceBo bo, PageQuery pageQuery);

    /**
     * 查询应用数据源列表
     */
    List<SysSheetSourceVo> queryList(SysSheetSourceBo bo);

    /**
     * 新增应用数据源
     */
    Boolean insertByBo(SysSheetSourceBo bo);

    /**
     * 修改应用数据源
     */
    Boolean updateByBo(SysSheetSourceBo bo);

    /**
     * 校验并批量删除应用数据源信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 获取应用绑定的数据源
     * @param sheetId   应用ID
     * @return
     */
    List<SysSheetSourceVo> listDataSourceBySheetId(Long sheetId);
}
