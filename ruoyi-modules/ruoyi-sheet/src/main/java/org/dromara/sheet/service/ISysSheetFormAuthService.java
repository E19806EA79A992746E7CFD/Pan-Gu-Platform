package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.bo.SysSheetFormAuthBo;
import org.dromara.sheet.domain.vo.SysSheetFormAuthVo;

import java.util.Collection;
import java.util.List;

/**
 * 应用单授权Service接口
 *
 * @author Ysl
 * @date 2023-12-04
 */
public interface ISysSheetFormAuthService {

    /**
     * 查询应用单授权
     */
    SysSheetFormAuthVo queryById(Long formAuthId);

    /**
     * 查询应用单授权列表
     */
    TableDataInfo<SysSheetFormAuthVo> queryPageList(SysSheetFormAuthBo bo, PageQuery pageQuery);

    /**
     * 查询应用单授权列表
     */
    List<SysSheetFormAuthVo> queryList(SysSheetFormAuthBo bo);

    /**
     * 新增应用单授权
     */
    Boolean insertByBo(SysSheetFormAuthBo bo);

    /**
     * 修改应用单授权
     */
    Boolean updateByBo(SysSheetFormAuthBo bo);

    /**
     * 校验并批量删除应用单授权信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

}
