package org.dromara.sheet.domain.bo;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysProvideService;

/**
 * 应用控件业务对象 sys_provide_service
 *
 * @author Ysl
 * @date 2023-11-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysProvideService.class, reverseConvertGenerate = false)
public class SysProvideServiceBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotNull(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long serviceId;

    /**
     * 服务名称
     */
    @NotBlank(message = "服务名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 服务编码
     */
    @NotBlank(message = "服务编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 接口域名
     */
    @NotBlank(message = "接口域名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String apiDomain;

    /**
     * 是否启用，使用字典
     */
    @NotBlank(message = "是否启用，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 排序号
     */
    private Long sort;

    /**
     * 备注
     */
    private String remark;


}
