package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;

/**
 * 列表OSS数据DTO
 * @author Ysl
 * @date 2023/9/11 19:26
 * @describe
 */
@Data
public class DataOssDto {

    /**
     * 单据ID
     */
    private Long orderId;

    /**
     * ossID
     */
    private String ossId;

    /**
     * ossUrl
     */
    private String ossUrl;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 租户ID
     */
    private String tenantId;
}
