package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysSheetFormCheck;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 单校验视图对象 sys_sheet_form_check
 *
 * @author Ysl
 * @date 2023-09-16
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetFormCheck.class)
public class SysSheetFormCheckVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long sheetFormCheckId;

    /**
     * 表单ID
     */
    @ExcelProperty(value = "表单ID")
    private Long formId;

    /**
     * 表字段ID
     */
    @ExcelProperty(value = "表字段ID")
    private Long sheetFieldId;

    /**
     * 字段中文名称
     */
    @ExcelProperty(value = "字段中文名称")
    private String sheetFieldName;

    /**
     * 字段名称
     */
    @ExcelProperty(value = "字段名称")
    private String sheetField;

    /**
     * 校验类型，使用字典，1.必填，2.长度，3.正则，4接口
     */
    @ExcelProperty(value = "校验类型，使用字典，1.必填，2.长度，3.正则，4接口", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "form_check_type")
    private String checkType;

    /**
     * 错误提示
     */
    @ExcelProperty(value = "错误提示")
    private String errorMsg;

    /**
     * 最小长度
     */
    @ExcelProperty(value = "最小长度")
    private Long minLength;

    /**
     * 最大长度
     */
    @ExcelProperty(value = "最大长度")
    private Long maxLength;

    /**
     * 正则表达式
     */
    @ExcelProperty(value = "正则表达式")
    private String regular;

    /**
     * 校验接口模板ID
     */
    @ExcelProperty(value = "校验接口模板ID")
    private Long apiTemplateId;

    /**
     * 校验接口名称
     */
    @ExcelProperty(value = "校验接口名称")
    private String apiTemplateName;

    /**
     * 校验接口地址
     */
    @ExcelProperty(value = "校验接口地址")
    private String apiTemplateUrl;


}
