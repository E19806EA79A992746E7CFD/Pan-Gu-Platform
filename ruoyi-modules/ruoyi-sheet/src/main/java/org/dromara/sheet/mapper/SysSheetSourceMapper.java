package org.dromara.sheet.mapper;

import org.dromara.sheet.domain.SysSheetSource;
import org.dromara.sheet.domain.vo.SysSheetSourceVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 应用数据源Mapper接口
 *
 * @author Ysl
 * @date 2023-07-26
 */
public interface SysSheetSourceMapper extends BaseMapperPlus<SysSheetSource, SysSheetSourceVo> {

}
