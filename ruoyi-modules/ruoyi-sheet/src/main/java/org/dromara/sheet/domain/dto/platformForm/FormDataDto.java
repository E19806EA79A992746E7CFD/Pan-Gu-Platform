package org.dromara.sheet.domain.dto.platformForm;

import lombok.Data;
import org.dromara.sheet.domain.vo.SysSheetFormAttachmentVo;
import org.dromara.sheet.domain.vo.SysSheetSelectVo;

import java.util.List;
import java.util.Map;

/**
 * 表单数据DTO
 * @author yangshanlin
 * @date 2023/9/18 17:40
 * @describe
 */
@Data
public class FormDataDto {

    /**
     * 表单ID
     */
    private Long formId;

    /**
     * 字段值
     * key: 字段
     * value: 值
     */
    private List<Map<String, Object>> fieldValue;

    /**
     * 下拉字段选择数据
     */
    private List<SysSheetSelectVo> selectList;

    /**
     * 附件信息
     */
    private List<SysSheetFormAttachmentVo> formAttachmentList;

}
