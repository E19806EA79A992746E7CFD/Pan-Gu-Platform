package org.dromara.sheet.service;

import org.dromara.sheet.domain.SysCheckTemplate;
import org.dromara.sheet.domain.vo.SysCheckTemplateVo;
import org.dromara.sheet.domain.bo.SysCheckTemplateBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 校验模板维护Service接口
 *
 * @author zqx
 * @date 2023-09-04
 */
public interface ISysCheckTemplateService {

    /**
     * 查询校验模板维护
     */
    SysCheckTemplateVo queryById(Long checkTemplateId);

    /**
     * 查询校验模板维护列表
     */
    TableDataInfo<SysCheckTemplateVo> queryPageList(SysCheckTemplateBo bo, PageQuery pageQuery);

    /**
     * 查询校验模板维护列表
     */
    List<SysCheckTemplateVo> queryList(SysCheckTemplateBo bo);

    /**
     * 新增校验模板维护
     */
    Boolean insertByBo(SysCheckTemplateBo bo);

    /**
     * 修改校验模板维护
     */
    Boolean updateByBo(SysCheckTemplateBo bo);

    /**
     * 校验并批量删除校验模板维护信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
