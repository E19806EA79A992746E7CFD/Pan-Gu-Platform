package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;
import org.apache.poi.ss.usermodel.Sheet;
import org.dromara.sheet.domain.vo.SysSheetVo;

import java.util.List;
import java.util.Map;

/**
 * 平台通用列表数据DTO
 */
@Data
public class TableDataDto {

    /**
     * 总记录数
     */
    private long total;

    /**
     * 表头数据
     */
    private List<TableHeadDto> headList;

    /**
     * 列表数据
     */
    private List<Map<String, Object>> dataList;
}
