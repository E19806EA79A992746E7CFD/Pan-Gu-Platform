package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.common.translation.annotation.Translation;
import org.dromara.sheet.domain.bo.SysSheetButtonBo;
import org.dromara.sheet.domain.vo.SysSheetButtonVo;
import org.dromara.sheet.domain.vo.SysSheetFieldVo;
import org.dromara.sheet.service.ISysSheetButtonService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 应用控件
 *
 * @author Ysl
 * @date 2023-09-01
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/sheetButton")
public class SysSheetButtonController extends BaseController {

    private final ISysSheetButtonService sysSheetButtonService;

    /**
     * 查询应用控件列表
     */
    @SaCheckPermission("system:sheetButton:list")
    @GetMapping("/list")
    public TableDataInfo<SysSheetButtonVo> list(SysSheetButtonBo bo, PageQuery pageQuery) {
        return sysSheetButtonService.queryPageList(bo, pageQuery);
    }

    /**
     * 获取所有应用控件
     * @param bo
     * @return
     */
    @SaCheckPermission("system:sheetButton:list")
    @GetMapping("/listAll")
    public R<List<SysSheetButtonVo>> listAll(SysSheetButtonBo bo) {
        return R.ok(sysSheetButtonService.queryList(bo));
    }

    /**
     * 导出应用控件列表
     */
    @SaCheckPermission("system:sheetButton:export")
    @Log(title = "应用控件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysSheetButtonBo bo, HttpServletResponse response) {
        List<SysSheetButtonVo> list = sysSheetButtonService.queryList(bo);
        ExcelUtil.exportExcel(list, "应用控件", SysSheetButtonVo.class, response);
    }

    /**
     * 获取应用控件详细信息
     *
     * @param sheetFormButtonId 主键
     */
    @SaCheckPermission("system:sheetButton:query")
    @GetMapping("/{sheetFormButtonId}")
    public R<SysSheetButtonVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long sheetFormButtonId) {
        return R.ok(sysSheetButtonService.queryById(sheetFormButtonId));
    }

    /**
     * 新增应用控件
     */
    @SaCheckPermission("system:sheetButton:add")
    @Log(title = "应用控件", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysSheetButtonBo bo) {
        return toAjax(sysSheetButtonService.insertByBo(bo));
    }

    /**
     * 修改应用控件
     */
    @SaCheckPermission("system:sheetButton:edit")
    @Log(title = "应用控件", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysSheetButtonBo bo) {
        return toAjax(sysSheetButtonService.updateByBo(bo));
    }

    /**
     * 删除应用控件
     *
     * @param sheetFormButtonIds 主键串
     */
    @SaCheckPermission("system:sheetButton:remove")
    @Log(title = "应用控件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sheetFormButtonIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] sheetFormButtonIds) {
        return toAjax(sysSheetButtonService.deleteWithValidByIds(List.of(sheetFormButtonIds), true));
    }

    /**
     * 修改或保存应用控件
     * @param list
     * @return
     */
    @Transactional
    @Log(title = "应用控件", businessType = BusinessType.UPDATE)
    @PostMapping("/addOrUpdateSheetButton")
    public R<Void> addOrUpdateSheetButton(@RequestBody List<SysSheetButtonVo> list) {

        return toAjax(sysSheetButtonService.addOrUpdateSheetButton(list));
    }
}
