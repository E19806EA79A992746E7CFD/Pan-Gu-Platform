package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 应用对象 sys_sheet
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet")
public class SysSheet extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "sheet_id")
    private Long sheetId;

    /**
     * 父级ID
     */
    private Long pid;

    /**
     * 应用名称
     */
    private String name;

    /**
     * 标题
     */
    private String titleName;

    /**
     * 简称
     */
    private String shortName;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表主键
     */
    private String tableId;

    /**
     * 主表关联字段
     */
    private String parentName;

    /**
     * 应用状态（0正常 1停用）
     */
    private String status;

    /**
     * 数据字典ID
     */
    private String dictId;

    /**
     * 自定义源ID
     */
    private String sourceId;

    /**
     * 左固定列
     */
    private Long leftFixedIndex;

    /**
     * 右固定列
     */
    private Long rightFixedIndex;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否树表
     */
    private String isTree;
}
