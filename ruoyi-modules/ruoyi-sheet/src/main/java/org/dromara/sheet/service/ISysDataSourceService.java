package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.bo.SysDataSourceBo;
import org.dromara.sheet.domain.dto.DataSourceDto;
import org.dromara.sheet.domain.vo.SysDataSourceVo;

import java.util.Collection;
import java.util.List;

/**
 * 数据源维护Service接口
 *
 * @author Mr.Ysl
 * @date 2023-06-25
 */
public interface ISysDataSourceService {

    /**
     * 查询数据源维护
     */
    SysDataSourceVo queryById(Long dataSourceId);

    /**
     * 查询数据源维护列表
     */
    TableDataInfo<SysDataSourceVo> queryPageList(SysDataSourceBo bo, PageQuery pageQuery);

    /**
     * 查询数据源维护列表
     */
    List<SysDataSourceVo> queryList(SysDataSourceBo bo);

    /**
     * 新增数据源维护
     */
    Boolean insertByBo(SysDataSourceBo bo);

    /**
     * 修改数据源维护
     */
    Boolean updateByBo(SysDataSourceBo bo);

    /**
     * 校验并批量删除数据源维护信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 查询表名是否存在
     * @param tableName
     * @return
     */
    int checkTableName(String tableName);

    /**
     * 查询表中是否有该字段
     *
     * @param tableName
     * @param tableId
     * @return
     */
    int checkTableField(String tableName, String tableId);

    /**
     * 查询数据源映射数据
     * @param dto
     * @param pageQuery
     * @return
     */
    TableDataInfo<DataSourceDto> querySourceDataList(DataSourceDto dto, PageQuery pageQuery);
}
