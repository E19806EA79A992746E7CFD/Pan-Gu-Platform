package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysButton;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 控件维护视图对象 sys_button
 *
 * @author Ysl
 * @date 2023-09-01
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysButton.class)
public class SysButtonVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long buttonId;

    /**
     * 名称
     */
    @ExcelProperty(value = "名称")
    private String name;

    /**
     * 编码
     */
    @ExcelProperty(value = "编码")
    private String code;

    /**
     * 按钮类型，使用字典
     */
    @ExcelProperty(value = "按钮类型，使用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_button_type")
    private String type;

    /**
     * 样式，使用字典
     */
    @ExcelProperty(value = "样式，使用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_button_style")
    private String style;

    /**
     * 是否启用，使用字典
     */
    @ExcelProperty(value = "是否启用，使用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String status;

    /**
     * 排序号
     */
    @ExcelProperty(value = "排序号")
    private Long sort;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 服务主键ID
     */
    private Long serviceId;

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 服务域名
     */
    private String apiDomain;

    /**
     * 服务接口地址
     */
    private String apiUrl;
}
