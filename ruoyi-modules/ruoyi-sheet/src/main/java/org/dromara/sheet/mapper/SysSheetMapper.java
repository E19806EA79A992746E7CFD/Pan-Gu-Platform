package org.dromara.sheet.mapper;

import org.dromara.sheet.domain.SysSheet;
import org.dromara.sheet.domain.vo.SysSheetVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 应用Mapper接口
 *
 * @author Ysl
 * @date 2023-07-26
 */
public interface SysSheetMapper extends BaseMapperPlus<SysSheet, SysSheetVo> {

}
