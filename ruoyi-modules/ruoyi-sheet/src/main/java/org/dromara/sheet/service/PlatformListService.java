package org.dromara.sheet.service;

import org.dromara.sheet.domain.dto.platformList.*;

import java.util.List;

/**
 * 平台通用列表服务
 * @author Ysl
PlatformListService
 */
public interface PlatformListService {

    /**
     * 获取平台列表按钮就搜索配置
     * @param sheetId   应用ID
     * @return
     */
    SearchReturnDto getSearchAndButtonConfig(Long sheetId);

    /**
     * 获取平台通用列表数据
     * @param paramsDto     请求参数
     * @return
     */
    TableDataDto getListData(RequestParamsDto paramsDto);

    /**
     * 获取列表按钮配置
     * @param sheetId
     * @return
     */
    List<SearchButtonDto> getSheetButtonConfig(Long sheetId);

    /**
     * 获取列表搜索配置
     * @param sheetId
     * @return
     */
    List<SearchConfigDto> getSearchConfigList(Long sheetId);

    /**
     * 获取列表表头数据
     * @param sheetId
     * @return
     */
    List<TableHeadDto> getTableHeadData(Long sheetId);

    /**
     * 获取平台通用列表搜索条件
     * @param paramsDto     请求参数
     * @return
     */
    SearchParamsDto getPlatformListSearchParams(RequestParamsDto paramsDto);

    /**
     * 批量删除平台通用列表（逻辑删除）
     * @param ids
     * @param sheetId
     * @return
     */
    int deleteByIds(String ids, Long sheetId);
}
