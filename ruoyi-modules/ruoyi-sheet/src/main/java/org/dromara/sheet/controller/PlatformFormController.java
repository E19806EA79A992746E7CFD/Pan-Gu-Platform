package org.dromara.sheet.controller;

import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.sheet.domain.dto.platformForm.RequestFormDto;
import org.dromara.sheet.domain.dto.platformForm.SheetFormInfoDto;
import org.dromara.sheet.service.PlatformFormService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * 平台通用表单接口
 *
 * @author yangshanlin
 * @date 2023/8/31 15:10
 * @describe
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/platform/form")
public class PlatformFormController {

    private final PlatformFormService platformFormService;

    /**
     * 获取表单详情
     * @param sheetId   应用ID
     * @return
     */
    @GetMapping("/getSheetFormInfo")
    public R<SheetFormInfoDto> getSheetFormInfo(Long sheetId, Long orderId) throws Exception {
        return R.ok(platformFormService.getSheetFormInfo(sheetId, orderId));
    }

    /**
     * 表单保存
     * @param formDto
     * @return
     * @throws Exception
     */
    @PostMapping("/postFormData")
    @RepeatSubmit
    @Transactional
    public R<Long> postFormData(@RequestBody RequestFormDto formDto) throws Exception {

        return platformFormService.postFormData(formDto);
    }

}
