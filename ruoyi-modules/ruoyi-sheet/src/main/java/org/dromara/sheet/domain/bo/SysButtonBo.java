package org.dromara.sheet.domain.bo;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysButton;

/**
 * 控件维护业务对象 sys_button
 *
 * @author Ysl
 * @date 2023-09-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysButton.class, reverseConvertGenerate = false)
public class SysButtonBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotNull(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long buttonId;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 编码
     */
    @NotBlank(message = "编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 按钮类型，使用字典
     */
    @NotBlank(message = "按钮类型，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String type;

    /**
     * 样式，使用字典
     */
    @NotNull(message = "样式，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String style;

    /**
     * 是否启用，使用字典
     */
    @NotBlank(message = "是否启用，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 排序号
     */
    private Long sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 服务主键ID
     */
    private Long serviceId;

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 服务域名
     */
    private String apiDomain;

    /**
     * 服务接口地址
     */
    private String apiUrl;
}
