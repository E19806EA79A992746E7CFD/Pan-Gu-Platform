package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetFormAttachment;
import org.dromara.sheet.domain.vo.SysSheetFormAttachmentVo;

/**
 * 单附件Mapper接口
 *
 * @author Ysl
 * @date 2023-09-18
 */
public interface SysSheetFormAttachmentMapper extends BaseMapperPlus<SysSheetFormAttachment, SysSheetFormAttachmentVo> {

}
