package org.dromara.sheet.domain;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dromara.common.tenant.core.TenantEntity;

import java.io.Serial;

/**
 * 应用字段对象 sys_sheet_field
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet_field")
public class SysSheetField extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "sheet_field_id")
    private Long sheetFieldId;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 字段名
     */
    private String fieldName;

    /**
     * 简体中文
     */
    private String chineseName;

    /**
     * 字段类型
     */
    private Integer fieldType;

    /**
     * 数据源类型
     */
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String dataSourceName;

    /**
     * 表单排序
     */
    private Integer formSort;

    /**
     * 列表排序
     */
    private Integer tableSort;

    /**
     * 列表字段宽度
     */
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private Integer tableWidth;

    /**
     * 列表是否启用
     */
    private String tableStatus;

    /**
     * 表单是否启用
     */
    private String formStatus;

    /**
     * 列表是否显示
     */
    private String showTable;

    /**
     * 表单是否显示
     */
    private String showForm;

    /**
     * 展示方式
     */
    private Integer showType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 字段分组
     */
    private String fieldGroup;

    /**
     * 字段提示
     */
    private String fieldTip;
}
