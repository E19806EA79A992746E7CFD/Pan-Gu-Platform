package org.dromara.sheet.domain.vo;

import org.dromara.sheet.domain.SysCheckTemplate;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 校验模板维护视图对象 sys_check_template
 *
 * @author zqx
 * @date 2023-09-04
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysCheckTemplate.class)
public class SysCheckTemplateVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long checkTemplateId;

    /**
     * 名称
     */
    @ExcelProperty(value = "名称")
    private String name;

    /**
     * 编码
     */
    @ExcelProperty(value = "编码")
    private String code;

    /**
     * 接口地址
     */
    @ExcelProperty(value = "接口地址")
    private String apiUrl;

    /**
     * 是否启用，使用字典
     */
    @ExcelProperty(value = "是否启用，使用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String status;

    /**
     * 排序号
     */
    @ExcelProperty(value = "排序号")
    private Integer sort;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
