package org.dromara.sheet.service;

import org.dromara.sheet.domain.bo.SysSheetButtonAuthBo;
import org.dromara.sheet.domain.dto.SheetFormAuthDataDto;
import org.dromara.sheet.domain.vo.SheetAuthTreeSelectVo;

import java.util.List;

/**
 * 应用授权接口服务
 * @author yangshanlin
 * @date 2023/12/7 15:48
 * @describe
 */
public interface SysSheetAuthService {

    /**
     * 获取应用按钮列表
     * @param roleId    角色ID
     * @return
     */
    SheetAuthTreeSelectVo getSheetButtonList(Long roleId);

    /**
     * 保存应用按钮授权
     * @param buttonAuthBoList
     * @return
     */
    boolean saveSheetAuth(List<SysSheetButtonAuthBo> buttonAuthBoList);

    /**
     * 保存应用表单授权
     * @param authData
     * @return
     */
    boolean saveSheetFormAuthData(SheetFormAuthDataDto authData);

    /**
     * 获取应用表单及按钮授权数据
     * @param roleId
     * @return
     */
    SheetFormAuthDataDto getSheetFormAuthData(Long roleId);

}
