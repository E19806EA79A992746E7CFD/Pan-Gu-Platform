package org.dromara.sheet.domain.dto.platformForm;

import lombok.Data;

import java.util.List;

/**
 * 表单页签DTO
 * @author yangshanlin
 * @date 2023/9/19 13:38
 * @describe
 */
@Data
public class TabDto {

    /**
     * 页签
     */
    private String tabName;

    /**
     * 表单ID
     */
    private List<Long> formIdList;
}
