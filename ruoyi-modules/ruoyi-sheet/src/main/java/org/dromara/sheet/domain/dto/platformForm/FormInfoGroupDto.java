package org.dromara.sheet.domain.dto.platformForm;

import lombok.Data;

import java.util.List;

/**
 * 表单信息分组DTO
 * @author yangshanlin
 * @date 2023/9/18 18:26
 * @describe
 */
@Data
public class FormInfoGroupDto {


    /**
     * 分组名称
     */
    private String groupName;

    /**
     * 字段信息
     */
    private List<FormFieldDto> formFieldList;
}
