package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.sheet.domain.bo.SysButtonBo;
import org.dromara.sheet.domain.vo.SysButtonVo;

import java.util.Collection;
import java.util.List;

/**
 * 控件维护Service接口
 *
 * @author Ysl
 * @date 2023-09-01
 */
public interface ISysButtonService {

    /**
     * 查询控件维护
     */
    SysButtonVo queryById(Long buttonId);

    /**
     * 查询控件维护列表
     */
    TableDataInfo<SysButtonVo> queryPageList(SysButtonBo bo, PageQuery pageQuery);

    /**
     * 查询控件维护列表
     */
    List<SysButtonVo> queryList(SysButtonBo bo);

    /**
     * 新增控件维护
     */
    Boolean insertByBo(SysButtonBo bo);

    /**
     * 修改控件维护
     */
    Boolean updateByBo(SysButtonBo bo);

    /**
     * 校验并批量删除控件维护信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
