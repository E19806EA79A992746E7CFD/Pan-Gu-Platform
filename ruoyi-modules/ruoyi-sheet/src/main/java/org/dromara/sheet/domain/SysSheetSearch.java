package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 应用搜索对象 sys_sheet_search
 *
 * @author Ysl
 * @date 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet_search")
public class SysSheetSearch extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "sheet_search_id")
    private Long sheetSearchId;

    /**
     *  应用ID
     */
    private Long sheetId;

    /**
     * 搜索类型，使用字典

     */
    private String searchType;

    /**
     * 搜索标题
     */
    private String searchName;

    /**
     * 搜索提示
     */
    private String searchTip;

    /**
     * 判断逻辑，使用字典
     */
    private String checkType;

    /**
     * 搜索字段ID，多个逗号隔开
     */
    private String searchFieldId;

    /**
     * 搜索字段，多个逗号隔开
     */
    private String searchFieldName;

    /**
     * 是否启用，使用字典
     */
    private String status;

    /**
     * 是否高级搜索，使用字典
     */
    private String seniorSearch;

    /**
     * 数据源类型【1：数据字典】【2：物理表】
     */
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    private String dataSourceName;

    /**
     * 是否主应用
     */
    private String isMaster;

    /**
     * 子应用名称
     */
    private Long childSheetId;

    /**
     * 子应用主键字段
     */
    private String childSheetKeyId;

    /**
     * 排序
     */
    private Integer sort;

}
