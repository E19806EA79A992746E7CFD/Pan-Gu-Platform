package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.sheet.domain.bo.SysSheetFormCheckBo;
import org.dromara.sheet.domain.vo.SysSheetFormCheckVo;
import org.dromara.sheet.service.ISysSheetFormCheckService;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 单校验
 *
 * @author Ysl
 * @date 2023-09-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/sheetFormCheck")
public class SysSheetFormCheckController extends BaseController {

    private final ISysSheetFormCheckService sysSheetFormCheckService;

    /**
     * 查询单校验列表
     */
    @SaCheckPermission("system:sheetFormCheck:list")
    @GetMapping("/list")
    public TableDataInfo<SysSheetFormCheckVo> list(SysSheetFormCheckBo bo, PageQuery pageQuery) {
        return sysSheetFormCheckService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出单校验列表
     */
    @SaCheckPermission("system:sheetFormCheck:export")
    @Log(title = "单校验", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysSheetFormCheckBo bo, HttpServletResponse response) {
        List<SysSheetFormCheckVo> list = sysSheetFormCheckService.queryList(bo);
        ExcelUtil.exportExcel(list, "单校验", SysSheetFormCheckVo.class, response);
    }

    /**
     * 获取单校验详细信息
     *
     * @param sheetFormCheckId 主键
     */
    @SaCheckPermission("system:sheetFormCheck:query")
    @GetMapping("/{sheetFormCheckId}")
    public R<SysSheetFormCheckVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long sheetFormCheckId) {
        return R.ok(sysSheetFormCheckService.queryById(sheetFormCheckId));
    }

    /**
     * 新增单校验
     */
    @SaCheckPermission("system:sheetFormCheck:add")
    @Log(title = "单校验", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysSheetFormCheckBo bo) {
        return toAjax(sysSheetFormCheckService.insertByBo(bo));
    }

    /**
     * 修改单校验
     */
    @SaCheckPermission("system:sheetFormCheck:edit")
    @Log(title = "单校验", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysSheetFormCheckBo bo) {
        return toAjax(sysSheetFormCheckService.updateByBo(bo));
    }

    /**
     * 删除单校验
     *
     * @param sheetFormCheckIds 主键串
     */
    @SaCheckPermission("system:sheetFormCheck:remove")
    @Log(title = "单校验", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sheetFormCheckIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] sheetFormCheckIds) {
        return toAjax(sysSheetFormCheckService.deleteWithValidByIds(List.of(sheetFormCheckIds), true));
    }
}
