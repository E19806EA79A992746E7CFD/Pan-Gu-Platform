package org.dromara.sheet.domain.bo;

import org.dromara.sheet.domain.SysSheetSource;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 应用数据源业务对象 sys_sheet_source
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetSource.class, reverseConvertGenerate = false)
public class SysSheetSourceBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotNull(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long sheetSourceId;

    /**
     * 应用ID
     */
    @NotNull(message = "应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sheetId;

    /**
     * 数据源类型
     */
    @NotNull(message = "数据源类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    @NotNull(message = "数据源ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    @NotBlank(message = "数据源名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String dataSourceName;

    /**
     * 映射字段
     */
    @NotBlank(message = "映射字段不能为空", groups = { AddGroup.class, EditGroup.class })
    private String crossField;


}
