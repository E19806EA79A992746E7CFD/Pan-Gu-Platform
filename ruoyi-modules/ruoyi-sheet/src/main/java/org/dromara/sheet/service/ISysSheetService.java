package org.dromara.sheet.service;

import org.dromara.sheet.domain.SysSheet;
import org.dromara.sheet.domain.vo.SysSheetVo;
import org.dromara.sheet.domain.bo.SysSheetBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 应用Service接口
 *
 * @author Ysl
 * @date 2023-07-26
 */
public interface ISysSheetService {

    /**
     * 查询应用
     */
    SysSheetVo queryById(Long sheetId);

    /**
     * 查询应用列表
     */
    TableDataInfo<SysSheetVo> queryPageList(SysSheetBo bo, PageQuery pageQuery);

    /**
     * 查询应用列表
     */
    List<SysSheetVo> queryList(SysSheetBo bo);

    /**
     * 新增应用
     */
    Boolean insertByBo(SysSheetBo bo);

    /**
     * 修改应用
     */
    Boolean updateByBo(SysSheetBo bo);

    /**
     * 校验并批量删除应用信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 是否存在菜单子节点
     *
     * @param sheetId 菜单ID
     * @return 结果 true 存在 false 不存在
     */
    boolean hasChildBySheetId(Long sheetId);
}
