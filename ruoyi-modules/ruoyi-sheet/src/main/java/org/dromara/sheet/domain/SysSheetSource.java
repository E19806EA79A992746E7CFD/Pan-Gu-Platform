package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 应用数据源对象 sys_sheet_source
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet_source")
public class SysSheetSource extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "sheet_source_id")
    private Long sheetSourceId;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 数据源类型
     */
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    private String dataSourceName;

    /**
     * 映射字段
     */
    private String crossField;


}
