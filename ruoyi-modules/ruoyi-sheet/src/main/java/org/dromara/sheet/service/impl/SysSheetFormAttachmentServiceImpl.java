package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.sheet.domain.SysSheetFormAttachment;
import org.dromara.sheet.domain.bo.SysSheetFormAttachmentBo;
import org.dromara.sheet.domain.vo.SysSheetFormAttachmentVo;
import org.dromara.sheet.mapper.SysSheetFormAttachmentMapper;
import org.dromara.sheet.service.ISysSheetFormAttachmentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 单附件Service业务层处理
 *
 * @author Ysl
 * @date 2023-09-18
 */
@RequiredArgsConstructor
@Service
public class SysSheetFormAttachmentServiceImpl implements ISysSheetFormAttachmentService {

    private final SysSheetFormAttachmentMapper baseMapper;

    /**
     * 查询单附件
     */
    @Override
    public SysSheetFormAttachmentVo queryById(Long sheetFormAttachmentId){
        return baseMapper.selectVoById(sheetFormAttachmentId);
    }

    /**
     * 查询单附件列表
     */
    @Override
    public TableDataInfo<SysSheetFormAttachmentVo> queryPageList(SysSheetFormAttachmentBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetFormAttachment> lqw = buildQueryWrapper(bo);
        Page<SysSheetFormAttachmentVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询单附件列表
     */
    @Override
    public List<SysSheetFormAttachmentVo> queryList(SysSheetFormAttachmentBo bo) {
        LambdaQueryWrapper<SysSheetFormAttachment> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysSheetFormAttachment> buildQueryWrapper(SysSheetFormAttachmentBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetFormAttachment> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getSheetId() != null, SysSheetFormAttachment::getSheetId, bo.getSheetId());
        lqw.eq(bo.getOrderId() != null, SysSheetFormAttachment::getOrderId, bo.getOrderId());
        lqw.eq(bo.getOssId() != null, SysSheetFormAttachment::getOssId, bo.getOssId());
        lqw.eq(StringUtils.isNotBlank(bo.getUrl()), SysSheetFormAttachment::getUrl, bo.getUrl());
        lqw.like(StringUtils.isNotBlank(bo.getFileName()), SysSheetFormAttachment::getFileName, bo.getFileName());
        return lqw;
    }

    /**
     * 新增单附件
     */
    @Override
    public Boolean insertByBo(SysSheetFormAttachmentBo bo) {
        SysSheetFormAttachment add = MapstructUtils.convert(bo, SysSheetFormAttachment.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setSheetFormAttachmentId(add.getSheetFormAttachmentId());
        }
        return flag;
    }

    /**
     * 修改单附件
     */
    @Override
    public Boolean updateByBo(SysSheetFormAttachmentBo bo) {
        SysSheetFormAttachment update = MapstructUtils.convert(bo, SysSheetFormAttachment.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetFormAttachment entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除单附件
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
