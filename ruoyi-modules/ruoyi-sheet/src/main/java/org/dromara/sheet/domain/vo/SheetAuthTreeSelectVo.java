package org.dromara.sheet.domain.vo;

import lombok.Data;
import org.dromara.sheet.domain.dto.platformList.SearchDataSourceDto;

import java.util.List;

/**
 *
 * @author yangshanlin
 * @date 2023/12/4 20:19
 * @describe
 */
@Data
public class SheetAuthTreeSelectVo {

    /**
     * 选中按钮列表
     */
    private List<Long> checkedKeys;

    /**
     * 应用按钮列表
     */
    List<SearchDataSourceDto> dataSourceList;
}
