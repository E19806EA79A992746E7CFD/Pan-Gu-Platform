package org.dromara.sheet.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yangshanlin
 * @date 2023/6/27 17:03
 * @describe
 */
@Data
public class DataSourceDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据源ID
     */
    private Long dataSourceId;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表ID
     */
    private String tableId;

    /**
     * 映射字段
     */
    private String crossField;

    /**
     * 映射字段值
     */
    private String crossFieldValue;
}
