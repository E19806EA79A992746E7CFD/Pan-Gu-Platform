package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetForm;
import org.dromara.sheet.domain.vo.SysSheetFormVo;

/**
 * 表单管理Mapper接口
 *
 * @author lhk
 * @date 2023-09-04
 */
public interface SysSheetFormMapper extends BaseMapperPlus<SysSheetForm, SysSheetFormVo> {

}
