package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.sheet.domain.bo.SysSheetFormAttachmentBo;
import org.dromara.sheet.domain.vo.SysSheetFormAttachmentVo;

import java.util.Collection;
import java.util.List;

/**
 * 单附件Service接口
 *
 * @author Ysl
 * @date 2023-09-18
 */
public interface ISysSheetFormAttachmentService {

    /**
     * 查询单附件
     */
    SysSheetFormAttachmentVo queryById(Long sheetFormAttachmentId);

    /**
     * 查询单附件列表
     */
    TableDataInfo<SysSheetFormAttachmentVo> queryPageList(SysSheetFormAttachmentBo bo, PageQuery pageQuery);

    /**
     * 查询单附件列表
     */
    List<SysSheetFormAttachmentVo> queryList(SysSheetFormAttachmentBo bo);

    /**
     * 新增单附件
     */
    Boolean insertByBo(SysSheetFormAttachmentBo bo);

    /**
     * 修改单附件
     */
    Boolean updateByBo(SysSheetFormAttachmentBo bo);

    /**
     * 校验并批量删除单附件信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
