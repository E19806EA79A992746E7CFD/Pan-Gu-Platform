package org.dromara.sheet.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * 字段展示类型
 * @author yangshanlin
 * @date 2023/11/7 10:48
 * @describe
 */
@Getter
@AllArgsConstructor
public enum FieldShowTypeEnum {

    /**
     * 普通单选
     */
    SINGLE(6),
    /**
     * 普通多选
     */
    MULTIPLE(7),
    /**
     * 级联单选
     */
    CASCADE_SINGLE(8),
    /**
     * 级联多选
     */
    CASCADE_MULTIPLE(9),
    /**
     * 树状单选
     */
    TREE_SINGLE(10),
    /**
     * 树状多选
     */
    TREE_MULTIPLE(11),
    /**
     * 图片类型
     */
    IMG_TYPE(17),
    /**
     * 文件类型
     */
    FILE_TYPE(18);

    private final Integer showType;

    /**
     * 获取所有筛选类型
     * @return
     */
    public static List<Integer> getAllSelectType() {
        return List.of(SINGLE.showType, MULTIPLE.showType, CASCADE_SINGLE.showType, CASCADE_MULTIPLE.showType, TREE_SINGLE.showType, TREE_MULTIPLE.showType);
    }

    /**
     * 树形筛选类型
     * @return
     */
    public static List<Integer> getTreeSelectType() {
        return List.of(CASCADE_SINGLE.showType, CASCADE_MULTIPLE.showType, TREE_SINGLE.showType, TREE_MULTIPLE.showType);
    }
}
