package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.sheet.domain.bo.SysCheckTemplateBo;
import org.dromara.sheet.domain.vo.SysCheckTemplateVo;
import org.dromara.sheet.domain.SysCheckTemplate;
import org.dromara.sheet.mapper.SysCheckTemplateMapper;
import org.dromara.sheet.service.ISysCheckTemplateService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 校验模板维护Service业务层处理
 *
 * @author zqx
 * @date 2023-09-04
 */
@RequiredArgsConstructor
@Service
public class SysCheckTemplateServiceImpl implements ISysCheckTemplateService {

    private final SysCheckTemplateMapper baseMapper;

    /**
     * 查询校验模板维护
     */
    @Override
    public SysCheckTemplateVo queryById(Long checkTemplateId){
        return baseMapper.selectVoById(checkTemplateId);
    }

    /**
     * 查询校验模板维护列表
     */
    @Override
    public TableDataInfo<SysCheckTemplateVo> queryPageList(SysCheckTemplateBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysCheckTemplate> lqw = buildQueryWrapper(bo);
        Page<SysCheckTemplateVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询校验模板维护列表
     */
    @Override
    public List<SysCheckTemplateVo> queryList(SysCheckTemplateBo bo) {
        LambdaQueryWrapper<SysCheckTemplate> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysCheckTemplate> buildQueryWrapper(SysCheckTemplateBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysCheckTemplate> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysCheckTemplate::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), SysCheckTemplate::getCode, bo.getCode());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysCheckTemplate::getStatus, bo.getStatus());
        lqw.orderByDesc(SysCheckTemplate::getSort);

        return lqw;
    }

    /**
     * 新增校验模板维护
     */
    @Override
    public Boolean insertByBo(SysCheckTemplateBo bo) {
        SysCheckTemplate add = MapstructUtils.convert(bo, SysCheckTemplate.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCheckTemplateId(add.getCheckTemplateId());
        }
        return flag;
    }

    /**
     * 修改校验模板维护
     */
    @Override
    public Boolean updateByBo(SysCheckTemplateBo bo) {
        SysCheckTemplate update = MapstructUtils.convert(bo, SysCheckTemplate.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysCheckTemplate entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除校验模板维护
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
