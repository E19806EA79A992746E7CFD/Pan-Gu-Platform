package org.dromara.sheet.domain.bo;

import org.dromara.sheet.domain.SysCheckTemplate;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 校验模板维护业务对象 sys_check_template
 *
 * @author zqx
 * @date 2023-09-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysCheckTemplate.class, reverseConvertGenerate = false)
public class SysCheckTemplateBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotNull(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long checkTemplateId;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 编码
     */
    @NotBlank(message = "编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 接口地址
     */
    @NotBlank(message = "接口地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String apiUrl;

    /**
     * 是否启用，使用字典
     */
    @NotBlank(message = "是否启用，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 排序号
     */
    @NotNull(message = "排序号不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer sort;

    /**
     * 备注
     */
    private String remark;


}
