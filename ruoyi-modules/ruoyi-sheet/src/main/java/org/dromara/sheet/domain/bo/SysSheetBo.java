package org.dromara.sheet.domain.bo;

import org.dromara.sheet.domain.SysSheet;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 应用业务对象 sys_sheet
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheet.class, reverseConvertGenerate = false)
public class SysSheetBo extends BaseEntity {

    /**
     * 主键ID
     */
    private Long sheetId;

    /**
     * 父级ID
     */
    @NotNull(message = "父级ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long pid;

    /**
     * 应用名称
     */
    @NotBlank(message = "应用名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 标题
     */
    private String titleName;

    /**
     * 简称
     */
    private String shortName;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表主键
     */
    private String tableId;

    /**
     * 主表关联字段
     */
    private String parentName;

    /**
     * 应用状态（0正常 1停用）
     */
    private String status;

    /**
     * 数据字典ID
     */
    private String dictId;

    /**
     * 自定义源ID
     */
    private String sourceId;

    /**
     * 左固定列
     */
    private Long leftFixedIndex;

    /**
     * 右固定列
     */
    private Long rightFixedIndex;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否树表
     */
    private String isTree;
}
