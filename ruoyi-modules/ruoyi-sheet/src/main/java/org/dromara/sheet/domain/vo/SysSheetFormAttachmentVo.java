package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysSheetFormAttachment;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 单附件视图对象 sys_sheet_form_attachment
 *
 * @author Ysl
 * @date 2023-09-18
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetFormAttachment.class)
public class SysSheetFormAttachmentVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long sheetFormAttachmentId;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private Long sheetId;

    /**
     * 字段ID
     */
    @ExcelProperty(value = "字段ID")
    private Long fieldId;

    /**
     * 单据ID
     */
    @ExcelProperty(value = "单据ID")
    private Long orderId;

    /**
     * OSSID
     */
    @ExcelProperty(value = "OSSID")
    private Long ossId;

    /**
     * url地址
     */
    @ExcelProperty(value = "url地址")
    private String url;

    /**
     * 文件名称
     */
    @ExcelProperty(value = "文件名称")
    private String fileName;

    /**
     * 文件后缀
     */
    private String fileSuffix;

    /**
     * 文件原名
     */
    private String originalName;

}
