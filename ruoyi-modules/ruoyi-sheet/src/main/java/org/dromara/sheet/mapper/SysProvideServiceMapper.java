package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysProvideService;
import org.dromara.sheet.domain.vo.SysProvideServiceVo;

/**
 * 应用控件Mapper接口
 *
 * @author Ysl
 * @date 2023-11-13
 */
public interface SysProvideServiceMapper extends BaseMapperPlus<SysProvideService, SysProvideServiceVo> {

}
