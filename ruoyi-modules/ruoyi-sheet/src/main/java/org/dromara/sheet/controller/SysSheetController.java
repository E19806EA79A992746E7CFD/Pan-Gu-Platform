package org.dromara.sheet.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.web.core.BaseController;
import org.dromara.sheet.domain.bo.SysSheetBo;
import org.dromara.sheet.domain.vo.SysSheetVo;
import org.dromara.sheet.service.ISysSheetService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 应用
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/sheet")
public class SysSheetController extends BaseController {

    private final ISysSheetService sysSheetService;

    /**
     * 查询应用列表
     */
    @SaCheckPermission("system:sheet:list")
    @GetMapping("/list")
    public R<List<SysSheetVo>> list(SysSheetBo bo) {
        List<SysSheetVo> sheetVoList = sysSheetService.queryList(bo);
        return R.ok(sheetVoList);
    }

    /**
     * 导出应用列表
     */
    @SaCheckPermission("system:sheet:export")
    @Log(title = "应用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysSheetBo bo, HttpServletResponse response) {
        List<SysSheetVo> list = sysSheetService.queryList(bo);
        ExcelUtil.exportExcel(list, "应用", SysSheetVo.class, response);
    }

    /**
     * 获取应用详细信息
     *
     * @param sheetId 主键
     */
    @GetMapping("/{sheetId}")
    @SaCheckPermission("system:sheet:query")
    public R<SysSheetVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long sheetId) {
        return R.ok(sysSheetService.queryById(sheetId));
    }

    /**
     * 新增应用
     */
    @SaCheckPermission("system:sheet:add")
    @Log(title = "应用", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysSheetBo bo) {
        return toAjax(sysSheetService.insertByBo(bo));
    }

    /**
     * 修改应用
     */
    @SaCheckPermission("system:sheet:edit")
    @Log(title = "应用", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysSheetBo bo) {
        return toAjax(sysSheetService.updateByBo(bo));
    }

    /**
     * 删除应用
     *
     * @param sheetId 主键
     */
    @SaCheckPermission("system:sheet:remove")
    @Log(title = "应用", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sheetId}")
    public R<Void> remove(@PathVariable("sheetId") Long sheetId) {
        if (sysSheetService.hasChildBySheetId(sheetId)) {
            return R.warn("存在子应用,不允许删除");
        }
        return toAjax(sysSheetService.deleteWithValidByIds(List.of(sheetId), true));
    }

    /**
     * 获取所有应用
     */
    @GetMapping("/listAll")
    public R<List<SysSheetVo>> listAll(SysSheetBo bo) {
        return R.ok(sysSheetService.queryList(bo));
    }
}
