package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 表单管理对象 sys_sheet_form
 *
 * @author lhk
 * @date 2023-09-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet_form")
public class SysSheetForm extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "sheet_form_id")
    private Long sheetFormId;

    /**
     * 父级ID
     */
    private Long pid;

    /**
     * 表单名称
     */
    private String path;

    /**
     * 表单名称
     */
    private String name;

    /**
     * 显示名称
     */
    private String showName;

    /**
     * 页签
     */
    private String tabName;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 应用名称
     */
    private String sheetName;

    /**
     * 排序号
     */
    private Long sort;

    /**
     * 显示方式
     */
    private Integer showType;

    /**
     * 是否启用
     */
    private String status;

    /**
     * 备注
     */
    private String remark;


}
