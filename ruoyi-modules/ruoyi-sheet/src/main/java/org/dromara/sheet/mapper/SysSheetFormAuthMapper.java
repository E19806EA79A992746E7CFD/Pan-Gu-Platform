package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetFormAuth;
import org.dromara.sheet.domain.vo.SysSheetFormAuthVo;

/**
 * 应用单授权Mapper接口
 *
 * @author Ysl
 * @date 2023-12-04
 */
public interface SysSheetFormAuthMapper extends BaseMapperPlus<SysSheetFormAuth, SysSheetFormAuthVo> {

}
