package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 校验模板维护对象 sys_check_template
 *
 * @author zqx
 * @date 2023-09-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_check_template")
public class SysCheckTemplate extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "check_template_id")
    private Long checkTemplateId;

    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 接口地址
     */
    private String apiUrl;

    /**
     * 是否启用，使用字典
     */
    private String status;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;


}
