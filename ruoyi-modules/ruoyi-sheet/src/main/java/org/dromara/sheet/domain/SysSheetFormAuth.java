package org.dromara.sheet.domain;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 应用单授权对象 sys_sheet_form_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@TableName("sys_sheet_form_auth")
public class SysSheetFormAuth {

    @Serial
    private static final long serialVersionUID = 1L;


    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 表单ID
     */
    private Long formId;

}
