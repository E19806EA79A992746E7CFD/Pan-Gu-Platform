package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.sheet.domain.SysDataSource;
import org.dromara.sheet.domain.bo.SysDataSourceBo;
import org.dromara.sheet.domain.dto.DataSourceDto;
import org.dromara.sheet.domain.vo.SysDataSourceVo;
import org.dromara.sheet.mapper.SysDataSourceMapper;
import org.dromara.sheet.service.ISysDataSourceService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 数据源维护Service业务层处理
 *
 * @author Mr.Ysl
 * @date 2023-06-25
 */
@RequiredArgsConstructor
@Service
public class SysDataSourceServiceImpl implements ISysDataSourceService {

    private final SysDataSourceMapper baseMapper;

    /**
     * 查询数据源维护
     */
    @Override
    public SysDataSourceVo queryById(Long dataSourceId){
        return baseMapper.selectVoById(dataSourceId);
    }

    /**
     * 查询数据源维护列表
     */
    @Override
    public TableDataInfo<SysDataSourceVo> queryPageList(SysDataSourceBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysDataSource> lqw = buildQueryWrapper(bo);
        Page<SysDataSourceVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询数据源维护列表
     */
    @Override
    public List<SysDataSourceVo> queryList(SysDataSourceBo bo) {
        LambdaQueryWrapper<SysDataSource> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysDataSource> buildQueryWrapper(SysDataSourceBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysDataSource> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysDataSource::getName, bo.getName());
        lqw.like(StringUtils.isNotBlank(bo.getTableName()), SysDataSource::getTableName, bo.getTableName());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysDataSource::getStatus, bo.getStatus());
        lqw.orderByDesc(SysDataSource::getCreateTime);
        return lqw;
    }

    /**
     * 新增数据源维护
     */
    @Override
    public Boolean insertByBo(SysDataSourceBo bo) {
        SysDataSource add = MapstructUtils.convert(bo, SysDataSource.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setDataSourceId(add.getDataSourceId());
        }
        return flag;
    }

    /**
     * 修改数据源维护
     */
    @Override
    public Boolean updateByBo(SysDataSourceBo bo) {
        SysDataSource update = MapstructUtils.convert(bo, SysDataSource.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysDataSource entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除数据源维护
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public int checkTableName(String tableName) {
        return baseMapper.checkTableName(tableName);
    }

    /**
     * 查询表中是否有该字段
     *
     * @param tableName
     * @param tableId
     * @return
     */
    @Override
    public int checkTableField(String tableName, String tableId) {
        List<String> columnList = baseMapper.selectTableColumn(tableName);
        return columnList.contains(tableId) ? 1 : 0 ;
    }

    /**
     * 查询数据源映射数据
     * @param dto
     * @param pageQuery
     * @return
     */
    @Override
    public TableDataInfo<DataSourceDto> querySourceDataList(DataSourceDto dto, PageQuery pageQuery) {
        SysDataSourceVo vo = baseMapper.selectVoById(dto.getDataSourceId());
        dto.setTableName(vo.getTableName());
        dto.setTableId(vo.getTableId());
        dto.setCrossField(vo.getCrossField());
        Page<DataSourceDto> result = baseMapper.querySourceDataList(pageQuery.build(), dto);
        return TableDataInfo.build(result);
    }
}
