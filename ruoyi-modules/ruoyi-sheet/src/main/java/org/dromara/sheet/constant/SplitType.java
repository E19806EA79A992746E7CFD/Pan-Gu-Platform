package org.dromara.sheet.constant;

/**
 * 分割符号
 * @author yangshanlin
 * @date 2023/11/10 18:16
 * @describe
 */
public interface SplitType {

    /**
     * 逗号
     */
    String SPLIT_COMMA = ",";

    /**
     * 分号
     */
    String SPLIT_SEMICOLON = ";";

    /**
     * 冒号
     */
    String SPLIT_COLON = ":";

    /**
     * 点
     */
    String SPLIT_DOT = ".";

    /**
     * 斜杠
     */
    String SPLIT_SLASH = "/";

    /**
     * 反斜杠
     */
    String SPLIT_BACKSLASH = "\\";

    /**
     * 下划线
     */
    String SPLIT_UNDERLINE = "_";
}
