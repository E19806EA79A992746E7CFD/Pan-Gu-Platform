package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 通用列表查询参数DTO
 * @author Ysl
 * @date 2023/9/11 18:42
 * @describe
 */
@Data
public class SearchParamsDto {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表主键
     */
    private String tableId;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 查询字段
     */
    private String fieldName;

    /**
     * 普通查询条件
     */
    private String inputParams;

    /**
     * 筛选查询条件
     */
    private List<Map<String,Object>> selectParams;

}
