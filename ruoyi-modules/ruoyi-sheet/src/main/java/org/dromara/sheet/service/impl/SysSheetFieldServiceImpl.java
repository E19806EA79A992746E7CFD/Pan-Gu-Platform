package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.sheet.domain.SysSheetFormFieldAuth;
import org.dromara.sheet.domain.vo.SysSheetFormFieldAuthVo;
import org.dromara.sheet.mapper.SysSheetFieldMapper;
import org.dromara.sheet.mapper.SysSheetFormFieldAuthMapper;
import org.dromara.sheet.service.ISysSheetFieldService;
import org.springframework.stereotype.Service;
import org.dromara.sheet.domain.bo.SysSheetFieldBo;
import org.dromara.sheet.domain.vo.SysSheetFieldVo;
import org.dromara.sheet.domain.SysSheetField;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 应用字段Service业务层处理
 *
 * @author Ysl
 * @date 2023-07-26
 */
@RequiredArgsConstructor
@Service
public class SysSheetFieldServiceImpl implements ISysSheetFieldService {

    private final SysSheetFieldMapper baseMapper;
    private final SysSheetFormFieldAuthMapper sheetFormFieldAuthMapper;

    /**
     * 查询应用字段
     */
    @Override
    public SysSheetFieldVo queryById(String sheetFiledId){
        return baseMapper.selectVoById(sheetFiledId);
    }

    /**
     * 查询应用字段列表
     */
    @Override
    public TableDataInfo<SysSheetFieldVo> queryPageList(SysSheetFieldBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetField> lqw = buildQueryWrapper(bo);
        Page<SysSheetFieldVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询应用字段列表
     */
    @Override
    public List<SysSheetFieldVo> queryList(SysSheetFieldBo bo) {
        LambdaQueryWrapper<SysSheetField> lqw = buildQueryWrapper(bo);
        List<SysSheetFieldVo> fieldList = baseMapper.selectVoList(lqw);

        if (bo.getRoleId() != null){
            List<SysSheetFormFieldAuthVo> authList = sheetFormFieldAuthMapper.selectVoList(
                new LambdaQueryWrapper<SysSheetFormFieldAuth>()
                    .eq(SysSheetFormFieldAuth::getRoleId, bo.getRoleId())
                    .eq(SysSheetFormFieldAuth::getSheetId, bo.getSheetId())
            );
            fieldList.forEach(field -> {
                authList.forEach(auth -> {
                    if (field.getSheetFieldId().equals(auth.getFieldId())){
                        field.setIsAuth(auth.getIsAuth());
                        field.setIsHidden(auth.getIsHidden());
                        field.setIsEdit(auth.getIsEdit());
                    }
                });
            });
        }
        return fieldList;
    }

    private LambdaQueryWrapper<SysSheetField> buildQueryWrapper(SysSheetFieldBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetField> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getSheetId() != null, SysSheetField::getSheetId, bo.getSheetId());
        lqw.like(StringUtils.isNotBlank(bo.getFieldName()), SysSheetField::getFieldName, bo.getFieldName());
        lqw.like(StringUtils.isNotBlank(bo.getChineseName()), SysSheetField::getChineseName, bo.getChineseName());
        lqw.eq(bo.getFieldType() != null, SysSheetField::getFieldType, bo.getFieldType());
        lqw.eq(bo.getDataSourceType() != null, SysSheetField::getDataSourceType, bo.getDataSourceType());
        lqw.eq(bo.getDataSourceId() != null, SysSheetField::getDataSourceId, bo.getDataSourceId());
        lqw.like(StringUtils.isNotBlank(bo.getDataSourceName()), SysSheetField::getDataSourceName, bo.getDataSourceName());
        lqw.eq(bo.getFormSort() != null, SysSheetField::getFormSort, bo.getFormSort());
        lqw.eq(bo.getTableSort() != null, SysSheetField::getTableSort, bo.getTableSort());
        lqw.eq(StringUtils.isNotBlank(bo.getTableStatus()), SysSheetField::getTableStatus, bo.getTableStatus());
        lqw.eq(StringUtils.isNotBlank(bo.getFormStatus()), SysSheetField::getFormStatus, bo.getFormStatus());
        lqw.eq(StringUtils.isNotBlank(bo.getShowTable()), SysSheetField::getShowTable, bo.getShowTable());
        lqw.eq(StringUtils.isNotBlank(bo.getShowForm()), SysSheetField::getShowForm, bo.getShowForm());
        lqw.eq(bo.getShowType() != null, SysSheetField::getShowType, bo.getShowType());
        lqw.orderByAsc(SysSheetField::getTableSort);
        return lqw;
    }

    /**
     * 新增应用字段
     */
    @Override
    public Boolean insertByBo(SysSheetFieldBo bo) {
        SysSheetField add = MapstructUtils.convert(bo, SysSheetField.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setSheetFieldId(add.getSheetFieldId());
        }
        return flag;
    }

    /**
     * 修改应用字段
     */
    @Override
    public Boolean updateByBo(SysSheetFieldBo bo) {
        SysSheetField update = MapstructUtils.convert(bo, SysSheetField.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetField entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除应用字段
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 批量修改
     *
     * @param list
     * @return
     */
    @Override
    public boolean updateBatchSheetField(List<SysSheetFieldVo> list) {
        List<SysSheetField> sheetFieldList = MapstructUtils.convert(list, SysSheetField.class);
        return baseMapper.updateBatchById(sheetFieldList);
    }
}
