package org.dromara.sheet.domain.dto.platformForm;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import org.dromara.sheet.domain.dto.platformList.SearchDataSourceDto;

import java.util.List;

/**
 * 表单字段DTO
 * @author yangshanlin
 * @date 2023/9/18 17:26
 * @describe
 */
@Data
public class FormFieldDto {

    /**
     * 主键ID
     */
    private Long sheetFieldId;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 字段名
     */
    private String fieldName;

    /**
     * 简体中文
     */
    private String chineseName;

    /**
     * 字段类型
     */
    private Integer fieldType;

    /**
     * 数据源ID
     */
    private Long dataSourceId;

    /**
     * 数据源类型
     */
    private Integer dataSourceType;

    /**
     * 字段数据源
     */
    private List<SearchDataSourceDto> sourceList;

    /**
     * 表单排序
     */
    private Integer formSort;

    /**
     * 表单是否启用
     */
    private String formStatus;

    /**
     * 表单是否显示
     */
    private String showForm;

    /**
     * 展示方式
     */
    private Integer showType;

    /**
     * 字段分组
     */
    private String fieldGroup;

    /**
     * 字段提示
     */
    private String fieldTip;

    /**
     * 是否授权
     */
    private String isAuth;

    /**
     * 是否隐藏
     */
    private String isHidden;

    /**
     * 是否可编辑
     */
    private String isEdit;
}
