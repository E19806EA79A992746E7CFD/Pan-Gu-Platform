package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.sheet.domain.SysSheetSelect;
import org.dromara.sheet.domain.bo.SysSheetSelectBo;
import org.dromara.sheet.domain.vo.SysSheetSelectVo;
import org.dromara.sheet.mapper.SysSheetSelectMapper;
import org.dromara.sheet.service.ISysSheetSelectService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 应用选择关联Service业务层处理
 *
 * @author Ysl
 * @date 2023-09-10
 */
@RequiredArgsConstructor
@Service
public class SysSheetSelectServiceImpl implements ISysSheetSelectService {

    private final SysSheetSelectMapper baseMapper;

    /**
     * 查询应用选择关联
     */
    @Override
    public SysSheetSelectVo queryById(Long formId){
        return baseMapper.selectVoById(formId);
    }

    /**
     * 查询应用选择关联列表
     */
    @Override
    public TableDataInfo<SysSheetSelectVo> queryPageList(SysSheetSelectBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetSelect> lqw = buildQueryWrapper(bo);
        Page<SysSheetSelectVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询应用选择关联列表
     */
    @Override
    public List<SysSheetSelectVo> queryList(SysSheetSelectBo bo) {
        LambdaQueryWrapper<SysSheetSelect> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysSheetSelect> buildQueryWrapper(SysSheetSelectBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetSelect> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getOrderId() != null, SysSheetSelect::getOrderId, bo.getOrderId());
        lqw.eq(bo.getSheetId() != null, SysSheetSelect::getSheetId, bo.getSheetId());
        lqw.eq(bo.getFieldId() != null, SysSheetSelect::getFieldId, bo.getFieldId());
        lqw.eq(bo.getSelectId() != null, SysSheetSelect::getSelectId, bo.getSelectId());
        lqw.eq(StringUtils.isNotBlank(bo.getPath()), SysSheetSelect::getPath, bo.getPath());
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysSheetSelect::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getShowPath()), SysSheetSelect::getShowPath, bo.getShowPath());
        return lqw;
    }

    /**
     * 新增应用选择关联
     */
    @Override
    public Boolean insertByBo(SysSheetSelectBo bo) {
        SysSheetSelect add = MapstructUtils.convert(bo, SysSheetSelect.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setOrderId(add.getOrderId());
        }
        return flag;
    }

    /**
     * 修改应用选择关联
     */
    @Override
    public Boolean updateByBo(SysSheetSelectBo bo) {
        SysSheetSelect update = MapstructUtils.convert(bo, SysSheetSelect.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetSelect entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除应用选择关联
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
