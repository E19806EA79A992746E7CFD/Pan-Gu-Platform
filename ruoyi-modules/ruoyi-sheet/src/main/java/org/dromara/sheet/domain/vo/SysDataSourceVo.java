package org.dromara.sheet.domain.vo;

import java.util.Date;

import org.dromara.common.translation.annotation.Translation;
import org.dromara.common.translation.constant.TransConstant;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysDataSource;

import java.io.Serial;
import java.io.Serializable;


/**
 * 数据源维护视图对象 sys_data_source
 *
 * @author Mr.Ysl
 * @date 2023-06-25
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysDataSource.class)
public class SysDataSourceVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    @ExcelProperty(value = "数据源名称")
    private String name;

    /**
     * 数据源表名称
     */
    @ExcelProperty(value = "数据源表名称")
    private String tableName;

    /**
     * 数据源表ID
     */
    @ExcelProperty(value = "数据源表ID")
    private String tableId;

    /**
     * 父级字段
     */
    private String parentId;

    /**
     * 路径字段（用于树状查询）
     */
    private String path;

    /**
     * 是否启用
     */
    @ExcelProperty(value = "是否启用", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String status;

    /**
     * 映射字段
     */
    @ExcelProperty(value = "映射字段")
    private String crossField;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 创建部门
     */
    @ExcelProperty(value = "创建部门")
    private Long createDept;


    /**
     * 创建人
     */
    @ExcelProperty(value = "创建人")
    private Long createBy;

    /**
     * 创建人名称
     */
    @Translation(type = TransConstant.USER_ID_TO_NAME, mapper = "createBy")
    private String createByName;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新人
     */
    @ExcelProperty(value = "更新人")
    private Long updateBy;

    /**
     * 更新人名称
     */
    @Translation(type = TransConstant.USER_ID_TO_NAME, mapper = "updateBy")
    private String updateByName;

    /**
     * 更新时间
     */
    @ExcelProperty(value = "更新时间")
    private Date updateTime;


}
