package org.dromara.sheet.domain.bo;

import org.dromara.sheet.domain.SysSheetField;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 应用字段业务对象 sys_sheet_field
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetField.class, reverseConvertGenerate = false)
public class SysSheetFieldBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotBlank(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long sheetFieldId;

    /**
     * 应用ID
     */
    @NotBlank(message = "应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sheetId;

    /**
     * 字段名
     */
    @NotBlank(message = "字段名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String fieldName;

    /**
     * 简体中文
     */
    @NotBlank(message = "简体中文不能为空", groups = { AddGroup.class, EditGroup.class })
    private String chineseName;

    /**
     * 字段类型
     */
    @NotNull(message = "字段类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer fieldType;

    /**
     * 数据源类型
     */
    @NotNull(message = "数据源类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    @NotNull(message = "数据源ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    @NotBlank(message = "数据源名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String dataSourceName;

    /**
     * 表单排序
     */
    @NotNull(message = "表单排序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer formSort;

    /**
     * 列表排序
     */
    @NotNull(message = "列表排序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer tableSort;

    /**
     * 列表字段宽度
     */
    private Integer tableWidth;

    /**
     * 列表是否启用
     */
    @NotBlank(message = "是否启用不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tableStatus;

    /**
     * 表单是否启用
     */
    @NotBlank(message = "是否启用不能为空", groups = { AddGroup.class, EditGroup.class })
    private String formStatus;

    /**
     * 列表是否显示
     */
    @NotBlank(message = "列表是否显示不能为空", groups = { AddGroup.class, EditGroup.class })
    private String showTable;

    /**
     * 表单是否显示
     */
    @NotBlank(message = "表单是否显示不能为空", groups = { AddGroup.class, EditGroup.class })
    private String showForm;

    /**
     * 展示方式
     */
    @NotNull(message = "展示方式不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer showType;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = { AddGroup.class, EditGroup.class })
    private String remark;

    /**
     * 字段分组
     */
    private String fieldGroup;

    /**
     * 字段提示
     */
    private String fieldTip;

    /**
     * 角色ID
     */
    private Long roleId;
}
