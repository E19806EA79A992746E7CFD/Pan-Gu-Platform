package org.dromara.sheet.domain.dto;

import lombok.Data;

/**
 * 系统用户
 */
@Data
public class SysUserDto {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 昵称
     */
    private String nickName;
}
