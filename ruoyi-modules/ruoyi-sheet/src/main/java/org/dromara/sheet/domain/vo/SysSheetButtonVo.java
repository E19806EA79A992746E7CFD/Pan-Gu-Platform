package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysSheetButton;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用控件视图对象 sys_sheet_button
 *
 * @author Ysl
 * @date 2023-09-01
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetButton.class)
public class SysSheetButtonVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long sheetButtonId;

    /**
     * 绑定ID，应用ID或者表单ID
     */
    @ExcelProperty(value = "绑定ID，应用ID或者表单ID")
    private Long bindId;

    /**
     * 控件ID
     */
    @ExcelProperty(value = "控件ID")
    private Long buttonId;


    /**
     * 控件编码
     */
    @ExcelProperty(value = "控件编码")
    private String buttonCode;

    /**
     * 按钮类型
     */
    private String buttonType;

    /**
     * 按钮图标
     */
    private String buttonIcon;

    /**
     * 显示名称
     */
    @ExcelProperty(value = "显示名称")
    private String showName;

    /**
     * 分组
     */
    @ExcelProperty(value = "分组")
    private String showGroup;

    /**
     * 显示方式，使用字典，表单、列表
     */
    @ExcelProperty(value = "显示方式，使用字典，表单、列表 ", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "form_show_type")
    private Integer showType;

    /**
     * 打开方式
     */
    @ExcelProperty(value = "打开方式", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_button_type")
    private Integer openType;

    /**
     * 是否启用，使用字典

     */
    @ExcelProperty(value = "是否启用，使用字典 ", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String status;

    /**
     * 显示位置，使用字典

     */
    @ExcelProperty(value = "显示位置，使用字典 ", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "button_show_area")
    private Integer showArea;

    /**
     * 接口域名
     */
    private String apiDomain;

    /**
     * 接口地址
     */
    @ExcelProperty(value = "接口地址")
    private String apiUrl;

    /**
     * 提示
     */
    @ExcelProperty(value = "提示")
    private String showTip;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 是否授权
     */
    private String isAuth;
}
