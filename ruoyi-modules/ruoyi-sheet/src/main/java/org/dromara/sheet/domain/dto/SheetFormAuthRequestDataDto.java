package org.dromara.sheet.domain.dto;

import lombok.Data;
import org.dromara.sheet.domain.bo.SysSheetButtonAuthBo;
import org.dromara.sheet.domain.vo.SysSheetFieldVo;
import org.dromara.sheet.domain.vo.SysSheetFormVo;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author yangshanlin
 * @date 2023/12/6 10:10
 * @describe
 */
@Data
public class SheetFormAuthRequestDataDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 授权表单
     */
    private List<SysSheetFormVo> sheetFormAuthList;

    /**
     * 授权字段
     */
    private List<SysSheetFieldVo> sheetFieldList;

    /**
     * 授权按钮
     */
    private List<SysSheetButtonAuthBo> sheetButtonAuthList;
}
