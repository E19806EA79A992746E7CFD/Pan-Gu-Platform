package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serial;

/**
 * 数据源维护对象 sys_data_source
 *
 * @author Mr.Ysl
 * @date 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_data_source")
public class SysDataSource extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "data_source_id")
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    private String name;

    /**
     * 数据源表名称
     */
    private String tableName;

    /**
     * 数据源表ID
     */
    private String tableId;

    /**
     * 父级字段
     */
    private String parentId;

    /**
     * 路径字段（用于树状查询）
     */
    private String path;

    /**
     * 是否启用
     */
    private String status;

    /**
     * 映射字段
     */
    private String crossField;

    /**
     * 备注
     */
    private String remark;


}
