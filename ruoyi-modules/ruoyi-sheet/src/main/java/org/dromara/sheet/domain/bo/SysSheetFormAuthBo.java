package org.dromara.sheet.domain.bo;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysSheetFormAuth;

/**
 * 应用单授权业务对象 sys_sheet_form_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetFormAuth.class, reverseConvertGenerate = false)
public class SysSheetFormAuthBo extends BaseEntity {


    /**
     * 角色ID
     */
    @NotNull(message = "角色ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long roleId;

    /**
     * 表单ID
     */
    @NotNull(message = "表单ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long formId;



}
