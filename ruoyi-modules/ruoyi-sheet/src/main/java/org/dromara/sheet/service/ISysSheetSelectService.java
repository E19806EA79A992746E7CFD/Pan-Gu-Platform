package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.sheet.domain.bo.SysSheetSelectBo;
import org.dromara.sheet.domain.vo.SysSheetSelectVo;

import java.util.Collection;
import java.util.List;

/**
 * 应用选择关联Service接口
 *
 * @author Ysl
 * @date 2023-09-10
 */
public interface ISysSheetSelectService {

    /**
     * 查询应用选择关联
     */
    SysSheetSelectVo queryById(Long formId);

    /**
     * 查询应用选择关联列表
     */
    TableDataInfo<SysSheetSelectVo> queryPageList(SysSheetSelectBo bo, PageQuery pageQuery);

    /**
     * 查询应用选择关联列表
     */
    List<SysSheetSelectVo> queryList(SysSheetSelectBo bo);

    /**
     * 新增应用选择关联
     */
    Boolean insertByBo(SysSheetSelectBo bo);

    /**
     * 修改应用选择关联
     */
    Boolean updateByBo(SysSheetSelectBo bo);

    /**
     * 校验并批量删除应用选择关联信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
