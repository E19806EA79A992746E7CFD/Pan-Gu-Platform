package org.dromara.sheet.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.SysSheetFormFieldAuth;
import org.dromara.sheet.domain.bo.SysSheetFormFieldAuthBo;
import org.dromara.sheet.domain.vo.SysSheetFormFieldAuthVo;
import org.dromara.sheet.mapper.SysSheetFormFieldAuthMapper;
import org.dromara.sheet.service.ISysSheetFormFieldAuthService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 应用单字段授权Service业务层处理
 *
 * @author Ysl
 * @date 2023-12-04
 */
@RequiredArgsConstructor
@Service
public class SysSheetFormFieldAuthServiceImpl implements ISysSheetFormFieldAuthService {

    private final SysSheetFormFieldAuthMapper baseMapper;

    /**
     * 查询应用单字段授权
     */
    @Override
    public SysSheetFormFieldAuthVo queryById(Long formFieldAuthId){
        return baseMapper.selectVoById(formFieldAuthId);
    }

    /**
     * 查询应用单字段授权列表
     */
    @Override
    public TableDataInfo<SysSheetFormFieldAuthVo> queryPageList(SysSheetFormFieldAuthBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetFormFieldAuth> lqw = buildQueryWrapper(bo);
        Page<SysSheetFormFieldAuthVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询应用单字段授权列表
     */
    @Override
    public List<SysSheetFormFieldAuthVo> queryList(SysSheetFormFieldAuthBo bo) {
        LambdaQueryWrapper<SysSheetFormFieldAuth> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysSheetFormFieldAuth> buildQueryWrapper(SysSheetFormFieldAuthBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetFormFieldAuth> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getFormId() != null, SysSheetFormFieldAuth::getFormId, bo.getFormId());
        lqw.eq(bo.getFieldId() != null, SysSheetFormFieldAuth::getFieldId, bo.getFieldId());
        lqw.eq(StringUtils.isNotBlank(bo.getIsAuth()), SysSheetFormFieldAuth::getIsAuth, bo.getIsAuth());
        lqw.eq(StringUtils.isNotBlank(bo.getIsHidden()), SysSheetFormFieldAuth::getIsHidden, bo.getIsHidden());
        lqw.eq(StringUtils.isNotBlank(bo.getIsEdit()), SysSheetFormFieldAuth::getIsEdit, bo.getIsEdit());
        return lqw;
    }

    /**
     * 新增应用单字段授权
     */
    @Override
    public Boolean insertByBo(SysSheetFormFieldAuthBo bo) {
        SysSheetFormFieldAuth add = MapstructUtils.convert(bo, SysSheetFormFieldAuth.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        return flag;
    }

    /**
     * 修改应用单字段授权
     */
    @Override
    public Boolean updateByBo(SysSheetFormFieldAuthBo bo) {
        SysSheetFormFieldAuth update = MapstructUtils.convert(bo, SysSheetFormFieldAuth.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetFormFieldAuth entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除应用单字段授权
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
