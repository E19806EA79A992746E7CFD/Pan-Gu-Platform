package org.dromara.sheet.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;

/**
 * 应用单字段授权对象 sys_sheet_form_field_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@TableName("sys_sheet_form_field_auth")
public class SysSheetFormFieldAuth {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 表表ID
     */
    private Long formId;

    /**
     * 应用字段表ID
     */
    private Long fieldId;

    /**
     * 是否授予，使用字典
     */
    private String isAuth;

    /**
     * 是否隐藏，使用字典
     */
    private String isHidden;

    /**
     * 是否编辑，是否用字典
     */
    private String isEdit;


}
