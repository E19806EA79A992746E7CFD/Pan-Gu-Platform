package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.bo.SysSheetFormFieldAuthBo;
import org.dromara.sheet.domain.vo.SysSheetFormFieldAuthVo;

import java.util.Collection;
import java.util.List;

/**
 * 应用单字段授权Service接口
 *
 * @author Ysl
 * @date 2023-12-04
 */
public interface ISysSheetFormFieldAuthService {

    /**
     * 查询应用单字段授权
     */
    SysSheetFormFieldAuthVo queryById(Long formFieldAuthId);

    /**
     * 查询应用单字段授权列表
     */
    TableDataInfo<SysSheetFormFieldAuthVo> queryPageList(SysSheetFormFieldAuthBo bo, PageQuery pageQuery);

    /**
     * 查询应用单字段授权列表
     */
    List<SysSheetFormFieldAuthVo> queryList(SysSheetFormFieldAuthBo bo);

    /**
     * 新增应用单字段授权
     */
    Boolean insertByBo(SysSheetFormFieldAuthBo bo);

    /**
     * 修改应用单字段授权
     */
    Boolean updateByBo(SysSheetFormFieldAuthBo bo);

    /**
     * 校验并批量删除应用单字段授权信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
