package org.dromara.sheet.domain;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 应用按钮授权对象 sys_sheet_button_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@TableName("sys_sheet_button_auth")
public class SysSheetButtonAuth {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 绑定ID，应用ID或者表单ID
     */
    private Long bindId;

    /**
     * 按钮ID
     */
    private Long sheetButtonId;

    /**
     * 类型，使用字典（列表、表单）
     */
    private Integer type;


}
