package org.dromara.sheet.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 应用选择关联对象 sys_sheet_select
 *
 * @author Ysl
 * @date 2023-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet_select")
public class SysSheetSelect extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 业务单据ID
     */
    private Long orderId;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 字段ID
     */
    private Long fieldId;

    /**
     * 选中的数据ID
     */
    private Long selectId;

    /**
     * 选中的数据项路径
     */
    private String path;

    /**
     * 选中的数据名称
     */
    private String name;

    /**
     * 回显路径
     */
    private String showPath;

    /**
     * 对应数据下标
     */
    @TableField("`index`")
    private Integer index;
}
