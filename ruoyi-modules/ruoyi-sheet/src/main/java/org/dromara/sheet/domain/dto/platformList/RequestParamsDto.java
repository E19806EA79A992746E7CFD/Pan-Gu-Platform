package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;

import java.io.Serial;
import java.util.List;

/**
 * 请求通用搜索参数DTO
 * @author Ysl
 */
@Data
public class RequestParamsDto {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 应用ID
     */
    private Long sheetId;

    /**
     * 第几页
     */
    private Integer pageNum;

    /**
     * 每页多少条
     */
    private Integer pageSize;

    /**
     * 搜索配置
     */
    private List<SearchFieldDto> fieldList;
}
