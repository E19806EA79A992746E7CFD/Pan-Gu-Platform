package org.dromara.sheet.domain.dto.platformForm;

import lombok.Data;
import org.dromara.sheet.domain.vo.SysSheetFormCheckVo;

import java.util.List;

/**
 * 表单校验字段DTO
 *
 * @author yangshanlin
 * @date 2023/9/18 18:18
 * @describe
 */
@Data
public class FormCheckFieldDto {

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 校验规则
     */
    List<SysSheetFormCheckVo> checkRuleList;
}
