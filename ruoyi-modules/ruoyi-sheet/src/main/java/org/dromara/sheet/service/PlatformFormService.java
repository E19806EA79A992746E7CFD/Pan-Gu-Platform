package org.dromara.sheet.service;

import org.dromara.common.core.domain.R;
import org.dromara.sheet.domain.dto.platformForm.RequestFormDto;
import org.dromara.sheet.domain.dto.platformForm.SheetFormInfoDto;

/**
 * 平台通用表单服务
 * @author Ysl
 * @date 2023-09-18
 */
public interface PlatformFormService {

    /**
     * 获取表单详情
     * @param sheetId   应用ID
     * @return
     */
    SheetFormInfoDto getSheetFormInfo(Long sheetId, Long orderId) throws Exception;

    /**
     * 表单保存
     * @param formDto
     * @return
     */
    R<Long> postFormData(RequestFormDto formDto) throws Exception;
}
