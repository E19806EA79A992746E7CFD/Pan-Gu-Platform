package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysButton;
import org.dromara.sheet.domain.vo.SysButtonVo;

/**
 * 控件维护Mapper接口
 *
 * @author Ysl
 * @date 2023-09-01
 */
public interface SysButtonMapper extends BaseMapperPlus<SysButton, SysButtonVo> {

}
