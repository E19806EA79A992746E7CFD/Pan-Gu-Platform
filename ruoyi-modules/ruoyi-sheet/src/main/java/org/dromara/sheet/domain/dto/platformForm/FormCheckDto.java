package org.dromara.sheet.domain.dto.platformForm;

import lombok.Data;

import java.util.List;

/**
 * 表单校验DTO
 *
 * @author yangshanlin
 * @date 2023/9/19 17:36
 * @describe
 */
@Data
public class FormCheckDto {

    /**
     * 表单ID
     */
    private Long formId;

    /**
     * 校验字段
     */
    private List<FormCheckFieldDto> fieldList;
}
