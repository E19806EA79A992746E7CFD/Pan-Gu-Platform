package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.sheet.domain.bo.SysProvideServiceBo;
import org.dromara.sheet.domain.vo.SysProvideServiceVo;
import org.dromara.sheet.service.ISysProvideServiceService;
import org.redisson.api.RList;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 应用控件
 *
 * @author Ysl
 * @date 2023-11-13
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/provideService")
public class SysProvideServiceController extends BaseController {

    private final ISysProvideServiceService sysProvideServiceService;

    /**
     * 查询应用控件列表
     */
    @SaCheckPermission("system:provideService:list")
    @GetMapping("/list")
    public TableDataInfo<SysProvideServiceVo> list(SysProvideServiceBo bo, PageQuery pageQuery) {
        return sysProvideServiceService.queryPageList(bo, pageQuery);
    }

    /**
     * 获取所有控件列表
     * @param bo
     * @return
     */
    @GetMapping("/listAll")
    public R<List<SysProvideServiceVo>> listAll(SysProvideServiceBo bo) {
        return R.ok(sysProvideServiceService.queryList(bo));
    }


    /**
     * 导出应用控件列表
     */
    @SaCheckPermission("system:provideService:export")
    @Log(title = "应用控件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysProvideServiceBo bo, HttpServletResponse response) {
        List<SysProvideServiceVo> list = sysProvideServiceService.queryList(bo);
        ExcelUtil.exportExcel(list, "应用控件", SysProvideServiceVo.class, response);
    }

    /**
     * 获取应用控件详细信息
     *
     * @param serviceId 主键
     */
    @SaCheckPermission("system:provideService:query")
    @GetMapping("/{serviceId}")
    public R<SysProvideServiceVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long serviceId) {
        return R.ok(sysProvideServiceService.queryById(serviceId));
    }

    /**
     * 新增应用控件
     */
    @SaCheckPermission("system:provideService:add")
    @Log(title = "应用控件", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysProvideServiceBo bo) {
        return toAjax(sysProvideServiceService.insertByBo(bo));
    }

    /**
     * 修改应用控件
     */
    @SaCheckPermission("system:provideService:edit")
    @Log(title = "应用控件", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysProvideServiceBo bo) {
        return toAjax(sysProvideServiceService.updateByBo(bo));
    }

    /**
     * 删除应用控件
     *
     * @param serviceIds 主键串
     */
    @SaCheckPermission("system:provideService:remove")
    @Log(title = "应用控件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{serviceIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] serviceIds) {
        return toAjax(sysProvideServiceService.deleteWithValidByIds(List.of(serviceIds), true));
    }
}
