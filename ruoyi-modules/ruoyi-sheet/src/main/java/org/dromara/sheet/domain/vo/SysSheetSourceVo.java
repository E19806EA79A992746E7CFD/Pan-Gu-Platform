package org.dromara.sheet.domain.vo;

import org.dromara.sheet.domain.SysSheetSource;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用数据源视图对象 sys_sheet_source
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetSource.class)
public class SysSheetSourceVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long sheetSourceId;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private Long sheetId;

    /**
     * 数据源类型
     */
    @ExcelProperty(value = "数据源类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "data_source_type")
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    @ExcelProperty(value = "数据源ID")
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    @ExcelProperty(value = "数据源名称")
    private String dataSourceName;

    /**
     * 映射字段
     */
    @ExcelProperty(value = "映射字段")
    private String crossField;


}
