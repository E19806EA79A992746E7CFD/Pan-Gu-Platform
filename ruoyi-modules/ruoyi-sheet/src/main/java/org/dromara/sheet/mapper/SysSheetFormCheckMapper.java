package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetFormCheck;
import org.dromara.sheet.domain.vo.SysSheetFormCheckVo;

/**
 * 单校验Mapper接口
 *
 * @author Ysl
 * @date 2023-09-16
 */
public interface SysSheetFormCheckMapper extends BaseMapperPlus<SysSheetFormCheck, SysSheetFormCheckVo> {

}
