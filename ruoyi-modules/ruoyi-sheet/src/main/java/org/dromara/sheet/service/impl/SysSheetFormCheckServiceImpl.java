package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.sheet.domain.SysSheetFormCheck;
import org.dromara.sheet.domain.bo.SysSheetFormCheckBo;
import org.dromara.sheet.domain.vo.SysSheetFormCheckVo;
import org.dromara.sheet.mapper.SysSheetFormCheckMapper;
import org.dromara.sheet.service.ISysSheetFormCheckService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 单校验Service业务层处理
 *
 * @author Ysl
 * @date 2023-09-16
 */
@RequiredArgsConstructor
@Service
public class SysSheetFormCheckServiceImpl implements ISysSheetFormCheckService {

    private final SysSheetFormCheckMapper baseMapper;

    /**
     * 查询单校验
     */
    @Override
    public SysSheetFormCheckVo queryById(Long sheetFormCheckId){
        return baseMapper.selectVoById(sheetFormCheckId);
    }

    /**
     * 查询单校验列表
     */
    @Override
    public TableDataInfo<SysSheetFormCheckVo> queryPageList(SysSheetFormCheckBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetFormCheck> lqw = buildQueryWrapper(bo);
        Page<SysSheetFormCheckVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询单校验列表
     */
    @Override
    public List<SysSheetFormCheckVo> queryList(SysSheetFormCheckBo bo) {
        LambdaQueryWrapper<SysSheetFormCheck> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysSheetFormCheck> buildQueryWrapper(SysSheetFormCheckBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetFormCheck> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getFormId() != null, SysSheetFormCheck::getFormId, bo.getFormId());
        lqw.eq(bo.getSheetFieldId() != null, SysSheetFormCheck::getSheetFieldId, bo.getSheetFieldId());
        lqw.like(StringUtils.isNotBlank(bo.getSheetFieldName()), SysSheetFormCheck::getSheetFieldName, bo.getSheetFieldName());
        lqw.eq(StringUtils.isNotBlank(bo.getSheetField()), SysSheetFormCheck::getSheetField, bo.getSheetField());
        lqw.eq(StringUtils.isNotBlank(bo.getCheckType()), SysSheetFormCheck::getCheckType, bo.getCheckType());
        lqw.eq(StringUtils.isNotBlank(bo.getErrorMsg()), SysSheetFormCheck::getErrorMsg, bo.getErrorMsg());
        lqw.eq(bo.getMinLength() != null, SysSheetFormCheck::getMinLength, bo.getMinLength());
        lqw.eq(bo.getMaxLength() != null, SysSheetFormCheck::getMaxLength, bo.getMaxLength());
        lqw.eq(StringUtils.isNotBlank(bo.getRegular()), SysSheetFormCheck::getRegular, bo.getRegular());
        lqw.eq(bo.getApiTemplateId() != null, SysSheetFormCheck::getApiTemplateId, bo.getApiTemplateId());
        lqw.like(StringUtils.isNotBlank(bo.getApiTemplateName()), SysSheetFormCheck::getApiTemplateName, bo.getApiTemplateName());
        lqw.eq(StringUtils.isNotBlank(bo.getApiTemplateUrl()), SysSheetFormCheck::getApiTemplateUrl, bo.getApiTemplateUrl());
        return lqw;
    }

    /**
     * 新增单校验
     */
    @Override
    public Boolean insertByBo(SysSheetFormCheckBo bo) {
        SysSheetFormCheck add = MapstructUtils.convert(bo, SysSheetFormCheck.class);
        if (add.getCheckType().equals("2")){
            add.setRegular(null);
            add.setApiTemplateId(null);
            add.setApiTemplateUrl(null);
            add.setApiTemplateName(null);
        } else if (add.getCheckType().equals("3")){
            add.setMaxLength(null);
            add.setMinLength(null);
            add.setApiTemplateId(null);
            add.setApiTemplateUrl(null);
            add.setApiTemplateName(null);
        } else if (add.getCheckType().equals("4")){
            add.setRegular(null);
            add.setMaxLength(null);
            add.setMinLength(null);
        }
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setSheetFormCheckId(add.getSheetFormCheckId());
        }
        return flag;
    }

    /**
     * 修改单校验
     */
    @Override
    public Boolean updateByBo(SysSheetFormCheckBo bo) {
        SysSheetFormCheck update = MapstructUtils.convert(bo, SysSheetFormCheck.class);
        if (update.getCheckType().equals("2")){
            update.setRegular(null);
            update.setApiTemplateId(null);
            update.setApiTemplateUrl(null);
            update.setApiTemplateName(null);
        } else if (update.getCheckType().equals("3")){
            update.setMaxLength(null);
            update.setMinLength(null);
            update.setApiTemplateId(null);
            update.setApiTemplateUrl(null);
            update.setApiTemplateName(null);
        } else if (update.getCheckType().equals("4")){
            update.setRegular(null);
            update.setMaxLength(null);
            update.setMinLength(null);
        }
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetFormCheck entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除单校验
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
