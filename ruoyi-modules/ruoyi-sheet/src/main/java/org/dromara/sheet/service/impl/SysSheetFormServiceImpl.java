package org.dromara.sheet.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.constant.CacheNames;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.helper.DataBaseHelper;
import org.dromara.common.redis.utils.CacheUtils;
import org.dromara.sheet.domain.SysSheetForm;
import org.dromara.sheet.domain.SysSheetFormAuth;
import org.dromara.sheet.domain.bo.SysSheetFormBo;
import org.dromara.sheet.domain.vo.SysSheetFormAuthVo;
import org.dromara.sheet.domain.vo.SysSheetFormVo;
import org.dromara.sheet.enums.YesOrNoEnum;
import org.dromara.sheet.mapper.SysSheetFormAuthMapper;
import org.dromara.sheet.mapper.SysSheetFormMapper;
import org.dromara.sheet.service.ISysSheetFormService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 表单管理Service业务层处理
 *
 * @author lhk
 * @date 2023-09-04
 */
@RequiredArgsConstructor
@Service
public class SysSheetFormServiceImpl implements ISysSheetFormService {

    private final SysSheetFormMapper baseMapper;
    private final SysSheetFormAuthMapper sheetFormAuthMapper;

    /**
     * 查询表单管理
     */
    @Override
    public SysSheetFormVo queryById(Long sheetFormId){
        return baseMapper.selectVoById(sheetFormId);
    }


    /**
     * 查询表单管理列表
     */
    @Override
    public List<SysSheetFormVo> queryList(SysSheetFormBo bo) {
        LambdaQueryWrapper<SysSheetForm> lqw = buildQueryWrapper(bo);
        List<SysSheetFormVo> list = baseMapper.selectVoList(lqw);

        // 查询角色授权表单
        if (bo.getRoleId() != null){
            List<SysSheetFormAuthVo> authList = sheetFormAuthMapper.selectVoList(
                new LambdaQueryWrapper<SysSheetFormAuth>()
                    .eq(SysSheetFormAuth::getRoleId, bo.getRoleId())
            );
            list.forEach(form -> {
                authList.forEach(auth -> {
                    if (form.getSheetFormId().equals(auth.getFormId())){
                        form.setIsAuth(YesOrNoEnum.YES.getCode());
                    }
                });
            });
        }
        return list;
    }

    private LambdaQueryWrapper<SysSheetForm> buildQueryWrapper(SysSheetFormBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetForm> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getPid() != null, SysSheetForm::getPid, bo.getPid());
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysSheetForm::getName, bo.getName());
        lqw.eq(bo.getSheetId() != null, SysSheetForm::getSheetId, bo.getSheetId());
        lqw.like(StringUtils.isNotBlank(bo.getSheetName()), SysSheetForm::getSheetName, bo.getSheetName());
        lqw.eq(bo.getSort() != null, SysSheetForm::getSort, bo.getSort());
        lqw.eq(bo.getShowType() != null, SysSheetForm::getShowType, bo.getShowType());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysSheetForm::getStatus, bo.getStatus());
        lqw.orderByAsc(SysSheetForm::getSort);
        return lqw;
    }

    /**
     * 新增表单管理
     */
    @Override
    public Boolean insertByBo(SysSheetFormBo bo) {
        SysSheetForm add = MapstructUtils.convert(bo, SysSheetForm.class);
        validEntityBeforeSave(add);

        if (!bo.getPid().equals(0L)){
            SysSheetForm info = baseMapper.selectById(bo.getPid());
            add.setPath(info.getPath() + StringUtils.SEPARATOR + bo.getPid());
        } else {
            add.setPath("0");
        }

        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setSheetFormId(add.getSheetFormId());
        }
        return flag;
    }

    /**
     * 修改表单管理
     */
    @Override
    public Boolean updateByBo(SysSheetFormBo bo) {
        SysSheetForm update = MapstructUtils.convert(bo, SysSheetForm.class);
        validEntityBeforeSave(update);

        SysSheetForm oldForm = baseMapper.selectById(bo.getSheetFormId());
        if (!update.getPid().equals(oldForm.getPid())) {
            SysSheetForm newParentForm = baseMapper.selectById(bo.getPid());
            if (ObjectUtil.isNotNull(newParentForm) && ObjectUtil.isNotNull(oldForm)) {
                String newPath = newParentForm.getPath() + StringUtils.SEPARATOR + newParentForm.getSheetFormId();
                String oldPath = oldForm.getPath();
                update.setPath(newPath);
                updateSheetFormChildren(update.getSheetFormId(), newPath, oldPath);
            }
        }

        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetForm entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除表单管理
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public boolean hasChildBySheetFormId(Long[] sheetFormIds) {
        return baseMapper.exists(new LambdaQueryWrapper<SysSheetForm>().eq(SysSheetForm::getPid,sheetFormIds[0]));
    }

    /**
     * 修改子元素关系
     *
     * @param sheetFormId   被修改的表单ID
     * @param newPath       新的父ID集合
     * @param oldPath       旧的父ID集合
     */
    private void updateSheetFormChildren(Long sheetFormId, String newPath, String oldPath) {
        List<SysSheetForm> children = baseMapper.selectList(new LambdaQueryWrapper<SysSheetForm>()
            .apply(DataBaseHelper.findInSet(sheetFormId, "path")));
        List<SysSheetForm> list = new ArrayList<>();
        for (SysSheetForm child : children) {
            SysSheetForm dept = new SysSheetForm();
            dept.setSheetFormId(child.getSheetFormId());
            dept.setPath(child.getPath().replaceFirst(oldPath, newPath));
            list.add(dept);
        }
        if (CollUtil.isNotEmpty(list)) {
            if (baseMapper.updateBatchById(list)) {
                list.forEach(dept -> CacheUtils.evict(CacheNames.SYS_DEPT, dept.getSheetFormId()));
            }
        }
    }
}
