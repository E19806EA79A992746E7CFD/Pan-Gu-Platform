package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetButton;
import org.dromara.sheet.domain.vo.SysSheetButtonVo;

/**
 * 应用控件Mapper接口
 *
 * @author Ysl
 * @date 2023-09-01
 */
public interface SysSheetButtonMapper extends BaseMapperPlus<SysSheetButton, SysSheetButtonVo> {

}
