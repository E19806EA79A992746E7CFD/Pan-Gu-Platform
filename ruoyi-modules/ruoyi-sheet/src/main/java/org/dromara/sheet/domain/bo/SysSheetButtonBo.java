package org.dromara.sheet.domain.bo;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysSheetButton;

/**
 * 应用控件业务对象 sys_sheet_button
 *
 * @author Ysl
 * @date 2023-09-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetButton.class, reverseConvertGenerate = false)
public class SysSheetButtonBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotNull(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long sheetButtonId;

    /**
     * 绑定ID，应用ID或者表单ID
     */
    private Long bindId;

    /**
     * 控件ID
     */
    @NotNull(message = "控件ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long buttonId;

    /**
     * 控件编码
     */
    @NotBlank(message = "控件编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String buttonCode;

    /**
     * 按钮类型
     */
    private String buttonType;

    /**
     * 按钮图标
     */
    private String buttonIcon;

    /**
     * 显示名称
     */
    @NotBlank(message = "显示名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String showName;

    /**
     * 分组
     */
    @NotBlank(message = "分组不能为空", groups = { AddGroup.class, EditGroup.class })
    private String showGroup;

    /**
     * 显示方式，使用字典，表单、列表
     */
    @NotNull(message = "显示方式，使用字典，表单、列表不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer showType;

    /**
     * 打开方式
     */
    private Integer openType;

    /**
     * 是否启用，使用字典

     */
    @NotBlank(message = "是否启用，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 显示位置，使用字典

     */
    @NotNull(message = "显示位置，使用字典不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer showArea;

    /**
     * 接口域名
     */
    private String apiDomain;

    /**
     * 接口地址
     */
    @NotBlank(message = "接口地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String apiUrl;

    /**
     * 提示
     */
    @NotBlank(message = "提示不能为空", groups = { AddGroup.class, EditGroup.class })
    private String showTip;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 角色ID
     */
    private Long roleId;
}
