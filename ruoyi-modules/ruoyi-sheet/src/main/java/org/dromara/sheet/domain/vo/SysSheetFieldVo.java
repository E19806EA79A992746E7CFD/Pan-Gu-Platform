package org.dromara.sheet.domain.vo;

import org.dromara.sheet.domain.SysSheetField;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用字段视图对象 sys_sheet_field
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetField.class)
public class SysSheetFieldVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long sheetFieldId;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private Long sheetId;

    /**
     * 字段名
     */
    @ExcelProperty(value = "字段名")
    private String fieldName;

    /**
     * 简体中文
     */
    @ExcelProperty(value = "简体中文")
    private String chineseName;

    /**
     * 字段类型
     */
    @ExcelProperty(value = "字段类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "table_field_type")
    private Integer fieldType;

    /**
     * 数据源类型
     */
    @ExcelProperty(value = "数据源类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "data_source_type")
    private Integer dataSourceType;

    /**
     * 数据源ID
     */
    @ExcelProperty(value = "数据源ID")
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    @ExcelProperty(value = "数据源名称")
    private String dataSourceName;

    /**
     * 表单排序
     */
    @ExcelProperty(value = "表单排序")
    private Integer formSort;

    /**
     * 列表排序
     */
    @ExcelProperty(value = "列表排序")
    private Integer tableSort;

    /**
     * 列表字段宽度
     */
    private Integer tableWidth;

    /**
     * 列表是否启用
     */
    @ExcelProperty(value = "列表是否启用", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String tableStatus;

    /**
     * 表单是否启用
     */
    @ExcelProperty(value = "表单是否启用", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String formStatus;

    /**
     * 列表是否显示
     */
    @ExcelProperty(value = "列表是否显示", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String showTable;

    /**
     * 表单是否显示
     */
    @ExcelProperty(value = "表单是否显示", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String showForm;

    /**
     * 展示方式
     */
    @ExcelProperty(value = "展示方式", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "field_show_type")
    private Integer showType;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 字段分组
     */
    @ExcelProperty(value = "字段分组")
    private String fieldGroup;

    /**
     * 字段提示
     */
    private String fieldTip;

    /**
     * 表单ID
     */
    private Long formId;

    /**
     * 是否授权
     */
    private String isAuth;

    /**
     * 是否隐藏
     */
    private String isHidden;

    /**
     * 是否可编辑
     */
    private String isEdit;
}
