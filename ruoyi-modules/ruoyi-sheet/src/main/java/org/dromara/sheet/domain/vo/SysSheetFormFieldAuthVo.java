package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import org.dromara.sheet.domain.SysSheetFormFieldAuth;

import java.io.Serial;
import java.io.Serializable;



/**
 * 应用单字段授权视图对象 sys_sheet_form_field_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetFormFieldAuth.class)
public class SysSheetFormFieldAuthVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 角色ID
     */
    @ExcelProperty(value = "角色ID")
    private Long roleId;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private Long sheetId;

    /**
     * 表单ID
     */
    @ExcelProperty(value = "表单ID")
    private Long formId;

    /**
     * 应用字段表ID
     */
    @ExcelProperty(value = "应用字段表ID")
    private Long fieldId;

    /**
     * 是否授予，使用字典
     */
    @ExcelProperty(value = "是否授予，使用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String isAuth;

    /**
     * 是否隐藏，使用字典
     */
    @ExcelProperty(value = "是否隐藏，使用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String isHidden;

    /**
     * 是否编辑，是否用字典
     */
    @ExcelProperty(value = "是否编辑，是否用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String isEdit;

    /**
     * 操作状态 I：新增 U：修改 D：删除
     */
    private String handleState;
}
