package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.sheet.domain.bo.SysButtonBo;
import org.dromara.sheet.domain.bo.SysSheetButtonBo;
import org.dromara.sheet.domain.vo.SysButtonVo;
import org.dromara.sheet.domain.vo.SysSheetButtonVo;
import org.dromara.sheet.service.ISysButtonService;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 控件维护
 *
 * @author Ysl
 * @date 2023-09-01
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/button")
public class SysButtonController extends BaseController {

    private final ISysButtonService sysButtonService;

    /**
     * 查询控件维护列表
     */
    @SaCheckPermission("system:button:list")
    @GetMapping("/list")
    public TableDataInfo<SysButtonVo> list(SysButtonBo bo, PageQuery pageQuery) {
        return sysButtonService.queryPageList(bo, pageQuery);
    }

    /**
     * 获取所有控件
     * @param bo
     * @return
     */
    @GetMapping("/listAll")
    public R<List<SysButtonVo>> listAll(SysButtonBo bo) {
        return R.ok(sysButtonService.queryList(bo));
    }

    /**
     * 导出控件维护列表
     */
    @SaCheckPermission("system:button:export")
    @Log(title = "控件维护", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysButtonBo bo, HttpServletResponse response) {
        List<SysButtonVo> list = sysButtonService.queryList(bo);
        ExcelUtil.exportExcel(list, "控件维护", SysButtonVo.class, response);
    }

    /**
     * 获取控件维护详细信息
     *
     * @param buttonId 主键
     */
    @SaCheckPermission("system:button:query")
    @GetMapping("/{buttonId}")
    public R<SysButtonVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long buttonId) {
        return R.ok(sysButtonService.queryById(buttonId));
    }

    /**
     * 新增控件维护
     */
    @SaCheckPermission("system:button:add")
    @Log(title = "控件维护", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysButtonBo bo) {
        return toAjax(sysButtonService.insertByBo(bo));
    }

    /**
     * 修改控件维护
     */
    @SaCheckPermission("system:button:edit")
    @Log(title = "控件维护", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysButtonBo bo) {
        return toAjax(sysButtonService.updateByBo(bo));
    }

    /**
     * 删除控件维护
     *
     * @param buttonIds 主键串
     */
    @SaCheckPermission("system:button:remove")
    @Log(title = "控件维护", businessType = BusinessType.DELETE)
    @DeleteMapping("/{buttonIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] buttonIds) {
        return toAjax(sysButtonService.deleteWithValidByIds(List.of(buttonIds), true));
    }
}
