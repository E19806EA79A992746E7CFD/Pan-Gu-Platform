package org.dromara.sheet.domain.bo;

import io.github.linpeilie.annotations.AutoMapper;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.sheet.domain.SysDataSource;

/**
 * 数据源维护业务对象 sys_data_source
 *
 * @author Mr.Ysl
 * @date 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysDataSource.class, reverseConvertGenerate = false)
public class SysDataSourceBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotNull(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long dataSourceId;

    /**
     * 数据源名称
     */
    @NotBlank(message = "数据源名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 数据源表名称
     */
    @NotBlank(message = "数据源表名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tableName;

    /**
     * 数据源表ID
     */
    @NotBlank(message = "数据源表ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tableId;

    /**
     * 父级字段
     */
    private String parentId;

    /**
     * 路径字段（用于树状查询）
     */
    private String path;

    /**
     * 是否启用
     */
    @NotBlank(message = "是否启用不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 映射字段
     */
    @NotBlank(message = "映射字段不能为空", groups = { AddGroup.class, EditGroup.class })
    private String crossField;

    /**
     * 备注
     */
    private String remark;


}
