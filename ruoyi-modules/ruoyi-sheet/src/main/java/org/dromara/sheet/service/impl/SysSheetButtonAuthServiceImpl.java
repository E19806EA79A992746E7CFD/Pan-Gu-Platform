package org.dromara.sheet.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.SysSheetButtonAuth;
import org.dromara.sheet.domain.bo.SysSheetButtonAuthBo;
import org.dromara.sheet.domain.vo.SysSheetButtonAuthVo;
import org.dromara.sheet.mapper.SysSheetButtonAuthMapper;
import org.dromara.sheet.service.ISysSheetButtonAuthService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 应用按钮授权Service业务层处理
 *
 * @author Ysl
 * @date 2023-12-04
 */
@RequiredArgsConstructor
@Service
public class SysSheetButtonAuthServiceImpl implements ISysSheetButtonAuthService {

    private final SysSheetButtonAuthMapper baseMapper;

    /**
     * 查询应用按钮授权
     */
    @Override
    public SysSheetButtonAuthVo queryById(Long roleId){
        return baseMapper.selectVoById(roleId);
    }

    /**
     * 查询应用按钮授权列表
     */
    @Override
    public TableDataInfo<SysSheetButtonAuthVo> queryPageList(SysSheetButtonAuthBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetButtonAuth> lqw = buildQueryWrapper(bo);
        Page<SysSheetButtonAuthVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询应用按钮授权列表
     */
    @Override
    public List<SysSheetButtonAuthVo> queryList(SysSheetButtonAuthBo bo) {
        LambdaQueryWrapper<SysSheetButtonAuth> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysSheetButtonAuth> buildQueryWrapper(SysSheetButtonAuthBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetButtonAuth> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getBindId() != null, SysSheetButtonAuth::getBindId, bo.getBindId());
        lqw.eq(bo.getSheetButtonId() != null, SysSheetButtonAuth::getSheetButtonId, bo.getSheetButtonId());
        lqw.eq(bo.getType() != null, SysSheetButtonAuth::getType, bo.getType());
        return lqw;
    }

    /**
     * 新增应用按钮授权
     */
    @Override
    public Boolean insertByBo(SysSheetButtonAuthBo bo) {
        SysSheetButtonAuth add = MapstructUtils.convert(bo, SysSheetButtonAuth.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setRoleId(add.getRoleId());
        }
        return flag;
    }

    /**
     * 修改应用按钮授权
     */
    @Override
    public Boolean updateByBo(SysSheetButtonAuthBo bo) {
        SysSheetButtonAuth update = MapstructUtils.convert(bo, SysSheetButtonAuth.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetButtonAuth entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除应用按钮授权
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

}
