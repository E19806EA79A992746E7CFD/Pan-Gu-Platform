package org.dromara.sheet.domain.dto.platformForm;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 应用表单详情DTO
 *
 * @author Ysl
 * @date 2023/9/18 17:09
 * @describe
 */
@Data
public class SheetFormInfoDto {

    /**
     * 页签信息
     */
    private List<TabDto> tabList;

    /**
     * 表单信息
     */
    private List<FormInfoDto> formInfoList;

    /**
     * 校验规则
     */
    private List<FormCheckDto> formCheckList;

    /**
     * 表单数据
     */
    private List<FormDataDto> formDataList;
}
