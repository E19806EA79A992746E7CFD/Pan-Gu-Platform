package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.sheet.domain.bo.SysSheetFormBo;
import org.dromara.sheet.domain.vo.SysSheetFormVo;
import org.dromara.sheet.service.ISysSheetFormService;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;

/**
 * 表单管理
 *
 * @author lhk
 * @date 2023-09-04
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/sheetForm")
public class SysSheetFormController extends BaseController {

    private final ISysSheetFormService sysSheetFormService;

    /**
     * 查询表单管理列表
     */
    @SaCheckPermission("system:sheetForm:list")
    @GetMapping("/list")
    public R<List<SysSheetFormVo>> list(SysSheetFormBo bo) {
        return R.ok(sysSheetFormService.queryList(bo));
    }

    /**
     * 导出表单管理列表
     */
    @SaCheckPermission("system:sheetForm:export")
    @Log(title = "表单管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysSheetFormBo bo, HttpServletResponse response) {
        List<SysSheetFormVo> list = sysSheetFormService.queryList(bo);
        ExcelUtil.exportExcel(list, "表单管理", SysSheetFormVo.class, response);
    }

    /**
     * 获取表单管理详细信息
     *
     * @param sheetFormId 主键
     */
    @SaCheckPermission("system:sheetForm:query")
    @GetMapping("/{sheetFormId}")
    public R<SysSheetFormVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long sheetFormId) {
        return R.ok(sysSheetFormService.queryById(sheetFormId));
    }

    /**
     * 新增表单管理
     */
    @SaCheckPermission("system:sheetForm:add")
    @Log(title = "表单管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysSheetFormBo bo) {
        return toAjax(sysSheetFormService.insertByBo(bo));
    }

    /**
     * 修改表单管理
     */
    @SaCheckPermission("system:sheetForm:edit")
    @Log(title = "表单管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysSheetFormBo bo) {
        return toAjax(sysSheetFormService.updateByBo(bo));
    }

    /**
     * 删除表单管理
     *
     * @param sheetFormIds 主键串
     */
    @SaCheckPermission("system:sheetForm:remove")
    @Log(title = "表单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sheetFormIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] sheetFormIds) {
        if (sysSheetFormService.hasChildBySheetFormId(sheetFormIds)) {
            return R.warn("存在子表单,不允许删除");
        }
        return toAjax(sysSheetFormService.deleteWithValidByIds(List.of(sheetFormIds), true));
    }
}
