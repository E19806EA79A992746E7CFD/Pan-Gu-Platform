package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysSheetSelect;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用选择关联视图对象 sys_sheet_select
 *
 * @author Ysl
 * @date 2023-09-10
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetSelect.class)
public class SysSheetSelectVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 业务单据ID
     */
    @ExcelProperty(value = "业务单据ID")
    private Long orderId;

    /**
     * 表单ID
     */
    @ExcelProperty(value = "表单ID")
    private Long formId;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private Long sheetId;

    /**
     * 字段ID
     */
    @ExcelProperty(value = "字段ID")
    private Long fieldId;

    /**
     * 选中的数据ID
     */
    @ExcelProperty(value = "选中的数据ID")
    private Long selectId;

    /**
     * 选中的数据项路径
     */
    @ExcelProperty(value = "选中的数据项路径")
    private String path;

    /**
     * 选中的数据名称
     */
    @ExcelProperty(value = "选中的数据名称")
    private String name;

    /**
     * 回显路径
     */
    @ExcelProperty(value = "回显路径")
    private String showPath;

    /**
     * 对应数据下标
     */
    private Integer index;
}
