package org.dromara.sheet.domain.bo;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysSheetSelect;

/**
 * 应用选择关联业务对象 sys_sheet_select
 *
 * @author Ysl
 * @date 2023-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetSelect.class, reverseConvertGenerate = false)
public class SysSheetSelectBo extends BaseEntity {

    /**
     * 业务单据ID
     */
    @NotNull(message = "业务单据ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long orderId;

    /**
     * 应用ID
     */
    @NotNull(message = "应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sheetId;

    /**
     * 字段ID
     */
    @NotNull(message = "字段ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long fieldId;

    /**
     * 选中的数据ID
     */
    @NotNull(message = "选中的数据ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long selectId;

    /**
     * 选中的数据项路径
     */
    @NotBlank(message = "选中的数据项路径不能为空", groups = { AddGroup.class, EditGroup.class })
    private String path;

    /**
     * 选中的数据名称
     */
    @NotBlank(message = "选中的数据名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 回显路径
     */
    @NotBlank(message = "回显路径不能为空", groups = { AddGroup.class, EditGroup.class })
    private String showPath;

    /**
     * 对应数据下标
     */
    private Integer index;

}
