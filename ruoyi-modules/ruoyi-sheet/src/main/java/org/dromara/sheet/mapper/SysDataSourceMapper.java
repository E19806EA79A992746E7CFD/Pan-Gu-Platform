package org.dromara.sheet.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysDataSource;
import org.dromara.sheet.domain.dto.DataSourceDto;
import org.dromara.sheet.domain.vo.SysDataSourceVo;

import java.util.List;

/**
 * 数据源维护Mapper接口
 *
 * @author Mr.Ysl
 * @date 2023-06-25
 */
public interface SysDataSourceMapper extends BaseMapperPlus<SysDataSource, SysDataSourceVo> {

    /**
     * 查询表名是否存在
     * @param tableName
     * @return
     */
    @InterceptorIgnore(tenantLine = "true")
    int checkTableName(String tableName);

    /**
     * 获取表字段
     * @param tableName
     * @return
     */
    @InterceptorIgnore(tenantLine = "true")
    List<String> selectTableColumn(String tableName);

    /**
     * 查询数据源映射数据
     * @param page
     * @param dto
     * @return
     */
    @InterceptorIgnore(tenantLine = "true")
    Page<DataSourceDto> querySourceDataList(@Param("page") Page<DataSourceDto> page, @Param("dto") DataSourceDto dto);

    /**
     * 根据应用数据源表获取数据源信息
     * @param sheetSourceId
     * @return
     */
    SysDataSourceVo getDataSourceBySheetSourceId(Long sheetSourceId);
}
