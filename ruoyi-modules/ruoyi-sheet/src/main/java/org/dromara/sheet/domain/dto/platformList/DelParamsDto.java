package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 删除类DTO
 * @author yangshanlin
 * @date 2023/11/20 15:38
 * @describe
 */
@Data
public class DelParamsDto {

    /**
     * 租户号
     */
    private String tenantId;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 删除字段
     */
    private String delField;

    /**
     * 删除值
     */
    private List<Object> delValue;

    /**
     * 修改人
     */
    private Long updateBy;

    /**
     * 修改时间
     */
    private Date updateTime;
}
