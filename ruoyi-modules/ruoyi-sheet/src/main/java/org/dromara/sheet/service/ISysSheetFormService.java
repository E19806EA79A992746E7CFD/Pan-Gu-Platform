package org.dromara.sheet.service;


import org.dromara.sheet.domain.bo.SysSheetFormBo;
import org.dromara.sheet.domain.vo.SysSheetFormVo;

import java.util.Collection;
import java.util.List;

/**
 * 表单管理Service接口
 *
 * @author lhk
 * @date 2023-09-04
 */
public interface ISysSheetFormService {

    /**
     * 查询表单管理
     */
    SysSheetFormVo queryById(Long sheetFormId);


    /**
     * 查询表单管理列表
     */
    List<SysSheetFormVo> queryList(SysSheetFormBo bo);

    /**
     * 新增表单管理
     */
    Boolean insertByBo(SysSheetFormBo bo);

    /**
     * 修改表单管理
     */
    Boolean updateByBo(SysSheetFormBo bo);

    /**
     * 校验并批量删除表单管理信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    boolean hasChildBySheetFormId(Long[] sheetFormIds);
}
