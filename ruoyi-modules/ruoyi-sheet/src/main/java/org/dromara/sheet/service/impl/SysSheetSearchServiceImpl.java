package org.dromara.sheet.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.SysSheetSearch;
import org.dromara.sheet.domain.bo.SysSheetSearchBo;
import org.dromara.sheet.domain.dto.platformList.SearchConfigDto;
import org.dromara.sheet.domain.dto.platformList.SearchDataSourceDto;
import org.dromara.sheet.domain.vo.SysDataSourceVo;
import org.dromara.sheet.domain.vo.SysSheetSearchVo;
import org.dromara.sheet.mapper.SysDataSourceMapper;
import org.dromara.sheet.mapper.SysSheetSearchMapper;
import org.dromara.sheet.service.ISysSheetSearchService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 应用搜索Service业务层处理
 *
 * @author Ysl
 * @date 2023-08-31
 */
@RequiredArgsConstructor
@Service
public class SysSheetSearchServiceImpl implements ISysSheetSearchService {

    private final SysSheetSearchMapper baseMapper;
    private final SysDataSourceMapper dataSourceMapper;

    /**
     * 查询应用搜索
     */
    @Override
    public SysSheetSearchVo queryById(Long sheetSearchId){
        return baseMapper.selectVoById(sheetSearchId);
    }

    /**
     * 查询应用搜索列表
     */
    @Override
    public TableDataInfo<SysSheetSearchVo> queryPageList(SysSheetSearchBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetSearch> lqw = buildQueryWrapper(bo);
        Page<SysSheetSearchVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询应用搜索列表
     */
    @Override
    public List<SysSheetSearchVo> queryList(SysSheetSearchBo bo) {
        LambdaQueryWrapper<SysSheetSearch> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysSheetSearch> buildQueryWrapper(SysSheetSearchBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetSearch> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getSheetId() != null, SysSheetSearch::getSheetId, bo.getSheetId());
        lqw.eq(StringUtils.isNotBlank(bo.getSearchType()), SysSheetSearch::getSearchType, bo.getSearchType());
        lqw.like(StringUtils.isNotBlank(bo.getSearchName()), SysSheetSearch::getSearchName, bo.getSearchName());
        lqw.eq(StringUtils.isNotBlank(bo.getCheckType()), SysSheetSearch::getCheckType, bo.getCheckType());
        lqw.eq(StringUtils.isNotBlank(bo.getSearchFieldId()), SysSheetSearch::getSearchFieldId, bo.getSearchFieldId());
        lqw.like(StringUtils.isNotBlank(bo.getSearchFieldName()), SysSheetSearch::getSearchFieldName, bo.getSearchFieldName());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysSheetSearch::getStatus, bo.getStatus());
        lqw.eq(StringUtils.isNotBlank(bo.getSeniorSearch()), SysSheetSearch::getSeniorSearch, bo.getSeniorSearch());
        lqw.eq(bo.getDataSourceType() != null, SysSheetSearch::getDataSourceType, bo.getDataSourceType());
        lqw.eq(bo.getDataSourceId() != null, SysSheetSearch::getDataSourceId, bo.getDataSourceId());
        lqw.like(StringUtils.isNotBlank(bo.getDataSourceName()), SysSheetSearch::getDataSourceName, bo.getDataSourceName());
        lqw.eq(StringUtils.isNotBlank(bo.getIsMaster()), SysSheetSearch::getIsMaster, bo.getIsMaster());
        lqw.eq(bo.getChildSheetId() != null, SysSheetSearch::getChildSheetId, bo.getChildSheetId());
        lqw.eq(StringUtils.isNotBlank(bo.getChildSheetKeyId()), SysSheetSearch::getChildSheetKeyId, bo.getChildSheetKeyId());
        lqw.orderByAsc(SysSheetSearch::getSort);
        lqw.orderByDesc(SysSheetSearch::getCreateTime);
        return lqw;
    }

    /**
     * 新增应用搜索
     */
    @Override
    public Boolean insertByBo(SysSheetSearchBo bo) {
        SysSheetSearch add = MapstructUtils.convert(bo, SysSheetSearch.class);

        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setSheetSearchId(add.getSheetSearchId());
        }
        return flag;
    }

    /**
     * 修改应用搜索
     */
    @Override
    public Boolean updateByBo(SysSheetSearchBo bo) {
        SysSheetSearch update = MapstructUtils.convert(bo, SysSheetSearch.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetSearch entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除应用搜索
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

}
