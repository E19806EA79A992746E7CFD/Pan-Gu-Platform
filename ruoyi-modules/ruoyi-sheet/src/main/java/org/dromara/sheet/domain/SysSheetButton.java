package org.dromara.sheet.domain;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 应用控件对象 sys_sheet_button
 *
 * @author Ysl
 * @date 2023-09-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sheet_button")
public class SysSheetButton extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "sheet_button_id")
    private Long sheetButtonId;

    /**
     * 绑定ID，应用ID或者表单ID
     */
    private Long bindId;

    /**
     * 控件ID
     */
    private Long buttonId;

    /**
     * 控件编码
     */
    private String buttonCode;

    /**
     * 按钮类型
     */
    private String buttonType;

    /**
     * 按钮图标
     */
    private String buttonIcon;

    /**
     * 显示名称
     */
    private String showName;

    /**
     * 分组
     */
    private String showGroup;

    /**
     * 显示方式，使用字典，表单、列表
     */
    private Integer showType;

    /**
     * 打开方式
     */
    private Integer openType;

    /**
     * 是否启用，使用字典

     */
    private String status;

    /**
     * 显示位置，使用字典

     */
    private Integer showArea;

    /**
     * 接口域名
     */
    private String apiDomain;

    /**
     * 接口地址
     */
    private String apiUrl;

    /**
     * 提示
     */
    private String showTip;

    /**
     * 排序号
     */
    private Integer sort;
}
