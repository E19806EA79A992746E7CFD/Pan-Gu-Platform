package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.sheet.domain.bo.SysProvideServiceBo;
import org.dromara.sheet.domain.vo.SysProvideServiceVo;

import java.util.Collection;
import java.util.List;

/**
 * 应用控件Service接口
 *
 * @author Ysl
 * @date 2023-11-13
 */
public interface ISysProvideServiceService {

    /**
     * 查询应用控件
     */
    SysProvideServiceVo queryById(Long serviceId);

    /**
     * 查询应用控件列表
     */
    TableDataInfo<SysProvideServiceVo> queryPageList(SysProvideServiceBo bo, PageQuery pageQuery);

    /**
     * 查询应用控件列表
     */
    List<SysProvideServiceVo> queryList(SysProvideServiceBo bo);

    /**
     * 新增应用控件
     */
    Boolean insertByBo(SysProvideServiceBo bo);

    /**
     * 修改应用控件
     */
    Boolean updateByBo(SysProvideServiceBo bo);

    /**
     * 校验并批量删除应用控件信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
