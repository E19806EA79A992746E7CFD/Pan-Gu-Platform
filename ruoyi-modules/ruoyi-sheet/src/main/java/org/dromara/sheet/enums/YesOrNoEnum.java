package org.dromara.sheet.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * 是否枚举
 * @author yangshanlin
 * @date 2023/11/7 10:48
 * @describe
 */
@Getter
@AllArgsConstructor
public enum YesOrNoEnum {

    /**
     * 是
     */
    YES("Y"),

    /**
     * 否
     */
    NO("N");


    private final String code;


}
