package org.dromara.sheet.domain.bo;

import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysSheetFormCheck;

/**
 * 单校验业务对象 sys_sheet_form_check
 *
 * @author Ysl
 * @date 2023-09-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetFormCheck.class, reverseConvertGenerate = false)
public class SysSheetFormCheckBo extends BaseEntity {

    /**
     * 主键ID
     */
    private Long sheetFormCheckId;

    /**
     * 表单ID
     */
    @NotNull(message = "表单ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long formId;

    /**
     * 表字段ID
     */
    @NotNull(message = "表字段ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sheetFieldId;

    /**
     * 字段中文名称
     */
    private String sheetFieldName;

    /**
     * 字段名称
     */
    private String sheetField;

    /**
     * 校验类型，使用字典，1.必填，2.长度，3.正则，4接口
     */
    @NotBlank(message = "校验类型，使用字典，1.必填，2.长度，3.正则，4接口不能为空", groups = { AddGroup.class, EditGroup.class })
    private String checkType;

    /**
     * 错误提示
     */
    @NotBlank(message = "错误提示不能为空", groups = { AddGroup.class, EditGroup.class })
    private String errorMsg;

    /**
     * 最小长度
     */
    private Long minLength;

    /**
     * 最大长度
     */
    private Long maxLength;

    /**
     * 正则表达式
     */
    private String regular;

    /**
     * 校验接口模板ID
     */
    private Long apiTemplateId;

    /**
     * 校验接口名称
     */
    private String apiTemplateName;

    /**
     * 校验接口地址
     */
    private String apiTemplateUrl;


}
