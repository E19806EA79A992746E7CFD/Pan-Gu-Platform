package org.dromara.sheet.service;

import org.dromara.sheet.domain.vo.SysSheetFieldVo;
import org.dromara.sheet.domain.bo.SysSheetFieldBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 应用字段Service接口
 *
 * @author Ysl
 * @date 2023-07-26
 */
public interface ISysSheetFieldService {

    /**
     * 查询应用字段
     */
    SysSheetFieldVo queryById(String sheetFiledId);

    /**
     * 查询应用字段列表
     */
    TableDataInfo<SysSheetFieldVo> queryPageList(SysSheetFieldBo bo, PageQuery pageQuery);

    /**
     * 查询应用字段列表
     */
    List<SysSheetFieldVo> queryList(SysSheetFieldBo bo);

    /**
     * 新增应用字段
     */
    Boolean insertByBo(SysSheetFieldBo bo);

    /**
     * 修改应用字段
     */
    Boolean updateByBo(SysSheetFieldBo bo);

    /**
     * 校验并批量删除应用字段信息
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    /**
     * 批量修改
     *
     * @param list
     * @return
     */
    boolean updateBatchSheetField(List<SysSheetFieldVo> list);
}
