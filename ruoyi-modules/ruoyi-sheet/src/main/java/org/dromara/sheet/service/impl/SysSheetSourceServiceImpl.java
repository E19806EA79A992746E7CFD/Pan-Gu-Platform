package org.dromara.sheet.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.sheet.mapper.SysSheetSourceMapper;
import org.dromara.sheet.service.ISysSheetSourceService;
import org.springframework.stereotype.Service;
import org.dromara.sheet.domain.bo.SysSheetSourceBo;
import org.dromara.sheet.domain.vo.SysSheetSourceVo;
import org.dromara.sheet.domain.SysSheetSource;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 应用数据源Service业务层处理
 *
 * @author Ysl
 * @date 2023-07-26
 */
@RequiredArgsConstructor
@Service
public class SysSheetSourceServiceImpl implements ISysSheetSourceService {

    private final SysSheetSourceMapper baseMapper;

    /**
     * 查询应用数据源
     */
    @Override
    public SysSheetSourceVo queryById(Long sheetSourceId){
        return baseMapper.selectVoById(sheetSourceId);
    }

    /**
     * 查询应用数据源列表
     */
    @Override
    public TableDataInfo<SysSheetSourceVo> queryPageList(SysSheetSourceBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetSource> lqw = buildQueryWrapper(bo);
        Page<SysSheetSourceVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询应用数据源列表
     */
    @Override
    public List<SysSheetSourceVo> queryList(SysSheetSourceBo bo) {
        LambdaQueryWrapper<SysSheetSource> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysSheetSource> buildQueryWrapper(SysSheetSourceBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetSource> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getSheetId() != null, SysSheetSource::getSheetId, bo.getSheetId());
        lqw.eq(bo.getDataSourceType() != null, SysSheetSource::getDataSourceType, bo.getDataSourceType());
        lqw.eq(bo.getDataSourceId() != null, SysSheetSource::getDataSourceId, bo.getDataSourceId());
        lqw.like(StringUtils.isNotBlank(bo.getDataSourceName()), SysSheetSource::getDataSourceName, bo.getDataSourceName());
        lqw.eq(StringUtils.isNotBlank(bo.getCrossField()), SysSheetSource::getCrossField, bo.getCrossField());
        return lqw;
    }

    /**
     * 新增应用数据源
     */
    @Override
    public Boolean insertByBo(SysSheetSourceBo bo) {
        SysSheetSource add = MapstructUtils.convert(bo, SysSheetSource.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setSheetSourceId(add.getSheetSourceId());
        }
        return flag;
    }

    /**
     * 修改应用数据源
     */
    @Override
    public Boolean updateByBo(SysSheetSourceBo bo) {
        SysSheetSource update = MapstructUtils.convert(bo, SysSheetSource.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetSource entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除应用数据源
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 获取应用绑定的数据源
     * @param sheetId   应用ID
     * @return
     */
    @Override
    public List<SysSheetSourceVo> listDataSourceBySheetId(Long sheetId) {
        return baseMapper.selectVoList(new LambdaQueryWrapper<SysSheetSource>().eq(SysSheetSource::getSheetId, sheetId));
    }
}
