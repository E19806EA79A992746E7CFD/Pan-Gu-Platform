package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.bo.SysSheetButtonAuthBo;
import org.dromara.sheet.domain.vo.SysSheetButtonAuthVo;

import java.util.Collection;
import java.util.List;

/**
 * 应用按钮授权Service接口
 *
 * @author Ysl
 * @date 2023-12-04
 */
public interface ISysSheetButtonAuthService {

    /**
     * 查询应用按钮授权
     */
    SysSheetButtonAuthVo queryById(Long roleId);

    /**
     * 查询应用按钮授权列表
     */
    TableDataInfo<SysSheetButtonAuthVo> queryPageList(SysSheetButtonAuthBo bo, PageQuery pageQuery);

    /**
     * 查询应用按钮授权列表
     */
    List<SysSheetButtonAuthVo> queryList(SysSheetButtonAuthBo bo);

    /**
     * 新增应用按钮授权
     */
    Boolean insertByBo(SysSheetButtonAuthBo bo);

    /**
     * 修改应用按钮授权
     */
    Boolean updateByBo(SysSheetButtonAuthBo bo);

    /**
     * 校验并批量删除应用按钮授权信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

}
