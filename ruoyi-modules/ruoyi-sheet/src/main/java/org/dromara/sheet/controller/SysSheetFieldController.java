package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.sheet.service.ISysSheetFieldService;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.sheet.domain.vo.SysSheetFieldVo;
import org.dromara.sheet.domain.bo.SysSheetFieldBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 应用字段
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/sheetField")
public class SysSheetFieldController extends BaseController {

    private final ISysSheetFieldService sysSheetFieldService;

    /**
     * 查询应用字段列表
     */
    @SaCheckPermission("system:sheetField:list")
    @GetMapping("/list")
    public TableDataInfo<SysSheetFieldVo> list(SysSheetFieldBo bo, PageQuery pageQuery) {
        return sysSheetFieldService.queryPageList(bo, pageQuery);
    }

    /**
     * 获取所有应用字段
     * @param bo
     * @return
     */
    @GetMapping("/listAll")
    public R<List<SysSheetFieldVo>> listAll(SysSheetFieldBo bo) {
        return R.ok(sysSheetFieldService.queryList(bo));
    }

    /**
     * 导出应用字段列表
     */
    @SaCheckPermission("system:sheetField:export")
    @Log(title = "应用字段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysSheetFieldBo bo, HttpServletResponse response) {
        List<SysSheetFieldVo> list = sysSheetFieldService.queryList(bo);
        ExcelUtil.exportExcel(list, "应用字段", SysSheetFieldVo.class, response);
    }

    /**
     * 获取应用字段详细信息
     *
     * @param sheetFiledId 主键
     */
    @SaCheckPermission("system:sheetField:query")
    @GetMapping("/{sheetFiledId}")
    public R<SysSheetFieldVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable String sheetFiledId) {
        return R.ok(sysSheetFieldService.queryById(sheetFiledId));
    }

    /**
     * 新增应用字段
     */
    @SaCheckPermission("system:sheetField:add")
    @Log(title = "应用字段", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysSheetFieldBo bo) {
        return toAjax(sysSheetFieldService.insertByBo(bo));
    }

    /**
     * 修改应用字段
     */
    @SaCheckPermission("system:sheetField:edit")
    @Log(title = "应用字段", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysSheetFieldBo bo) {
        return toAjax(sysSheetFieldService.updateByBo(bo));
    }

    /**
     * 删除应用字段
     *
     * @param sheetFiledIds 主键串
     */
    @SaCheckPermission("system:sheetField:remove")
    @Log(title = "应用字段", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sheetFiledIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable String[] sheetFiledIds) {
        return toAjax(sysSheetFieldService.deleteWithValidByIds(List.of(sheetFiledIds), true));
    }

    @Log(title = "应用字段", businessType = BusinessType.UPDATE)
    @PostMapping("/{updateBatchSheetField}")
    public R<Void> updateBatchSheetField(@RequestBody List<SysSheetFieldVo> list) {

        return toAjax(sysSheetFieldService.updateBatchSheetField(list));
    }
}
