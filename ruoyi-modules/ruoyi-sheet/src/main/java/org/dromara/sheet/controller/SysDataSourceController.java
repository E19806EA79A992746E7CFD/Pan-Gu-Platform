package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.bo.SysDataSourceBo;
import org.dromara.sheet.domain.dto.DataSourceDto;
import org.dromara.sheet.domain.vo.SysDataSourceVo;
import org.dromara.sheet.service.ISysDataSourceService;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;

/**
 * 数据源维护
 *
 * @author Mr.Ysl
 * @date 2023-06-25
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/dataSource")
public class SysDataSourceController extends BaseController {

    private final ISysDataSourceService sysDataSourceService;

    /**
     * 查询数据源维护列表
     */
    @SaCheckPermission("system:dataSource:list")
    @GetMapping("/list")
    public TableDataInfo<SysDataSourceVo> list(SysDataSourceBo bo, PageQuery pageQuery) {
        return sysDataSourceService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询数据源维护列表,不分页
     */
    @SaCheckPermission("system:dataSource:list")
    @GetMapping("/listAll")
    public R<List<SysDataSourceVo>> listAll(SysDataSourceBo bo) {
        return R.ok(sysDataSourceService.queryList(bo));
    }

    /**
     * 导出数据源维护列表
     */
    @SaCheckPermission("system:dataSource:export")
    @Log(title = "数据源维护", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysDataSourceBo bo, HttpServletResponse response) {
        List<SysDataSourceVo> list = sysDataSourceService.queryList(bo);
        ExcelUtil.exportExcel(list, "数据源维护", SysDataSourceVo.class, response);
    }

    /**
     * 获取数据源维护详细信息
     *
     * @param dataSourceId 主键
     */
    @SaCheckPermission("system:dataSource:query")
    @GetMapping("/{dataSourceId}")
    public R<SysDataSourceVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long dataSourceId) {
        return R.ok(sysDataSourceService.queryById(dataSourceId));
    }

    /**
     * 新增数据源维护
     */
    @SaCheckPermission("system:dataSource:add")
    @Log(title = "数据源维护", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysDataSourceBo bo) {
        return toAjax(sysDataSourceService.insertByBo(bo));
    }

    /**
     * 修改数据源维护
     */
    @SaCheckPermission("system:dataSource:edit")
    @Log(title = "数据源维护", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysDataSourceBo bo) {
        return toAjax(sysDataSourceService.updateByBo(bo));
    }

    /**
     * 删除数据源维护
     *
     * @param dataSourceIds 主键串
     */
    @SaCheckPermission("system:dataSource:remove")
    @Log(title = "数据源维护", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dataSourceIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] dataSourceIds) {
        return toAjax(sysDataSourceService.deleteWithValidByIds(List.of(dataSourceIds), true));
    }

    /**
     * 查询表名是否存在
     * @param tableName
     * @return
     */
    @PostMapping("/checkTableName")
    public R<Integer> checkTableName(String tableName) {
        return R.ok(sysDataSourceService.checkTableName(tableName));
    }

    /**
     * 查询表中是否有该字段
     * @param tableName
     * @param tableId
     * @return
     */
    @PostMapping("/checkTableField")
    public synchronized R<Integer> checkTableField(String tableName, String tableId) {
        return R.ok(sysDataSourceService.checkTableField(tableName, tableId));
    }

    /**
     * 查询数据源映射数据
     */
    @GetMapping("/sourceDataList")
    public TableDataInfo<DataSourceDto> sourceDataList(DataSourceDto dto, PageQuery pageQuery) {
        return sysDataSourceService.querySourceDataList(dto, pageQuery);
    }


}
