package org.dromara.sheet.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.sheet.domain.SysSheetButtonAuth;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用按钮授权视图对象 sys_sheet_button_auth
 *
 * @author Ysl
 * @date 2023-12-04
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheetButtonAuth.class)
public class SysSheetButtonAuthVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 角色ID
     */
    @ExcelProperty(value = "角色ID")
    private Long roleId;

    /**
     * 绑定ID，应用ID或者表单ID
     */
    @ExcelProperty(value = "绑定ID，应用ID或者表单ID")
    private Long bindId;

    /**
     * 按钮ID
     */
    @ExcelProperty(value = "按钮ID")
    private Long sheetButtonId;

    /**
     * 类型，使用字典（列表、表单）
     */
    @ExcelProperty(value = "类型，使用字典", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "form_show_type")
    private Integer type;

    /**
     * 操作状态 I：新增 U：修改 D：删除
     */
    private String handleState;
}
