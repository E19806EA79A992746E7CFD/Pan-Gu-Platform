package org.dromara.sheet.domain.dto.platformList;

import lombok.Data;

import java.io.Serial;

/**
 * 搜索字段参数DTO
 */
@Data
public class SearchFieldDto {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 搜索配置ID
     */
    private Long fieldId;

    /**
     * 搜索值
     */
    private String searchValue;
}
