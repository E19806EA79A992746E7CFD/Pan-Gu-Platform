package org.dromara.sheet.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.sheet.domain.SysSheetFormAuth;
import org.dromara.sheet.domain.bo.SysSheetFormAuthBo;
import org.dromara.sheet.domain.vo.SysSheetFormAuthVo;
import org.dromara.sheet.mapper.SysSheetFormAuthMapper;
import org.dromara.sheet.service.ISysSheetFormAuthService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 应用单授权Service业务层处理
 *
 * @author Ysl
 * @date 2023-12-04
 */
@RequiredArgsConstructor
@Service
public class SysSheetFormAuthServiceImpl implements ISysSheetFormAuthService {

    private final SysSheetFormAuthMapper baseMapper;

    /**
     * 查询应用单授权
     */
    @Override
    public SysSheetFormAuthVo queryById(Long formAuthId) {
        return baseMapper.selectVoById(formAuthId);
    }

    /**
     * 查询应用单授权列表
     */
    @Override
    public TableDataInfo<SysSheetFormAuthVo> queryPageList(SysSheetFormAuthBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysSheetFormAuth> lqw = buildQueryWrapper(bo);
        Page<SysSheetFormAuthVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询应用单授权列表
     */
    @Override
    public List<SysSheetFormAuthVo> queryList(SysSheetFormAuthBo bo) {
        LambdaQueryWrapper<SysSheetFormAuth> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysSheetFormAuth> buildQueryWrapper(SysSheetFormAuthBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysSheetFormAuth> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getRoleId() != null, SysSheetFormAuth::getRoleId, bo.getRoleId());
        lqw.eq(bo.getFormId() != null, SysSheetFormAuth::getFormId, bo.getFormId());
        return lqw;
    }

    /**
     * 新增应用单授权
     */
    @Override
    public Boolean insertByBo(SysSheetFormAuthBo bo) {
        SysSheetFormAuth add = MapstructUtils.convert(bo, SysSheetFormAuth.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        return flag;
    }

    /**
     * 修改应用单授权
     */
    @Override
    public Boolean updateByBo(SysSheetFormAuthBo bo) {
        SysSheetFormAuth update = MapstructUtils.convert(bo, SysSheetFormAuth.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysSheetFormAuth entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除应用单授权
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

}
