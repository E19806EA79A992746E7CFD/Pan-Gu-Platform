package org.dromara.sheet.domain.vo;

import org.dromara.sheet.domain.SysSheet;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 应用视图对象 sys_sheet
 *
 * @author Ysl
 * @date 2023-07-26
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysSheet.class)
public class SysSheetVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ExcelProperty(value = "主键ID")
    private Long sheetId;

    /**
     * 父级ID
     */
    @ExcelProperty(value = "父级ID")
    private Long pid;

    /**
     * 应用名称
     */
    @ExcelProperty(value = "应用名称")
    private String name;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String titleName;

    /**
     * 简称
     */
    @ExcelProperty(value = "简称")
    private String shortName;

    /**
     * 表名
     */
    @ExcelProperty(value = "表名")
    private String tableName;

    /**
     * 表主键
     */
    @ExcelProperty(value = "表主键")
    private String tableId;

    /**
     * 主表关联字段
     */
    private String parentName;

    /**
     * 应用状态（0正常 1停用）
     */
    @ExcelProperty(value = "应用状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType="sys_normal_disable")
    private String status;

    /**
     * 数据字典ID
     */
    private String dictId;

    /**
     * 自定义源ID
     */
    private String sourceId;

    /**
     * 左固定列
     */
    @ExcelProperty(value = "左固定列")
    private Long leftFixedIndex;

    /**
     * 右固定列
     */
    @ExcelProperty(value = "右固定列")
    private Long rightFixedIndex;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 是否树表
     */
    @ExcelProperty(value = "应用状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType="sys_yes_no")
    private String isTree;
}
