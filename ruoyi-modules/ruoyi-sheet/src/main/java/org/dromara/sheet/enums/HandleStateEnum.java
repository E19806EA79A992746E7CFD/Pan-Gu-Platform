package org.dromara.sheet.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 操作状态枚举
 * @author yangshanlin
 * @date 2023/12/9 16:59
 * @describe
 */
@Getter
@AllArgsConstructor
public enum HandleStateEnum {
    /**
     * 新增
     */
    INSERT("I"),
    /**
     * 修改
     */
    UPDATE("U"),
    /**
     * 删除
     */
    DELETE("D"),;

    private final String code;
}
