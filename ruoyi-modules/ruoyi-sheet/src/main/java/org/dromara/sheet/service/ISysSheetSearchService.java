package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.sheet.domain.bo.SysSheetSearchBo;
import org.dromara.sheet.domain.dto.platformList.SearchConfigDto;
import org.dromara.sheet.domain.vo.SysSheetSearchVo;

import java.util.Collection;
import java.util.List;

/**
 * 应用搜索Service接口
 *
 * @author Ysl
 * @date 2023-08-31
 */
public interface ISysSheetSearchService {

    /**
     * 查询应用搜索
     */
    SysSheetSearchVo queryById(Long sheetSearchId);

    /**
     * 查询应用搜索列表
     */
    TableDataInfo<SysSheetSearchVo> queryPageList(SysSheetSearchBo bo, PageQuery pageQuery);

    /**
     * 查询应用搜索列表
     */
    List<SysSheetSearchVo> queryList(SysSheetSearchBo bo);

    /**
     * 新增应用搜索
     */
    Boolean insertByBo(SysSheetSearchBo bo);

    /**
     * 修改应用搜索
     */
    Boolean updateByBo(SysSheetSearchBo bo);

    /**
     * 校验并批量删除应用搜索信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

}
