package org.dromara.sheet.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.sheet.domain.vo.SysCheckTemplateVo;
import org.dromara.sheet.domain.bo.SysCheckTemplateBo;
import org.dromara.sheet.service.ISysCheckTemplateService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 校验模板维护
 *
 * @author zqx
 * @date 2023-09-04
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/sheet/checkTemplate")
public class SysCheckTemplateController extends BaseController {

    private final ISysCheckTemplateService sysCheckTemplateService;

    /**
     * 查询校验模板维护列表
     */
    @SaCheckPermission("sheet:checkTemplate:list")
    @GetMapping("/list")
    public TableDataInfo<SysCheckTemplateVo> list(SysCheckTemplateBo bo, PageQuery pageQuery) {
        return sysCheckTemplateService.queryPageList(bo, pageQuery);
    }

    /**
     * 获取所有校验模板
     * @param bo
     * @return
     */
    @GetMapping("/listAll")
    public R<List<SysCheckTemplateVo>> listAll(SysCheckTemplateBo bo) {
        return R.ok(sysCheckTemplateService.queryList(bo));
    }

    /**
     * 导出校验模板维护列表
     */
    @SaCheckPermission("sheet:checkTemplate:export")
    @Log(title = "校验模板维护", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysCheckTemplateBo bo, HttpServletResponse response) {
        List<SysCheckTemplateVo> list = sysCheckTemplateService.queryList(bo);
        ExcelUtil.exportExcel(list, "校验模板维护", SysCheckTemplateVo.class, response);
    }

    /**
     * 获取校验模板维护详细信息
     *
     * @param checkTemplateId 主键
     */
    @SaCheckPermission("sheet:checkTemplate:query")
    @GetMapping("/{checkTemplateId}")
    public R<SysCheckTemplateVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long checkTemplateId) {
        return R.ok(sysCheckTemplateService.queryById(checkTemplateId));
    }

    /**
     * 新增校验模板维护
     */
    @SaCheckPermission("sheet:checkTemplate:add")
    @Log(title = "校验模板维护", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysCheckTemplateBo bo) {
        return toAjax(sysCheckTemplateService.insertByBo(bo));
    }

    /**
     * 修改校验模板维护
     */
    @SaCheckPermission("sheet:checkTemplate:edit")
    @Log(title = "校验模板维护", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysCheckTemplateBo bo) {
        return toAjax(sysCheckTemplateService.updateByBo(bo));
    }

    /**
     * 删除校验模板维护
     *
     * @param checkTemplateIds 主键串
     */
    @SaCheckPermission("sheet:checkTemplate:remove")
    @Log(title = "校验模板维护", businessType = BusinessType.DELETE)
    @DeleteMapping("/{checkTemplateIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] checkTemplateIds) {
        return toAjax(sysCheckTemplateService.deleteWithValidByIds(List.of(checkTemplateIds), true));
    }
}
