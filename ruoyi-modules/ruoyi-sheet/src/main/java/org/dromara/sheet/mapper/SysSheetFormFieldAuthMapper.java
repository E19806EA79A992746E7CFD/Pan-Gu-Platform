package org.dromara.sheet.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.sheet.domain.SysSheetFormFieldAuth;
import org.dromara.sheet.domain.vo.SysSheetFormFieldAuthVo;

/**
 * 应用单字段授权Mapper接口
 *
 * @author Ysl
 * @date 2023-12-04
 */
public interface SysSheetFormFieldAuthMapper extends BaseMapperPlus<SysSheetFormFieldAuth, SysSheetFormFieldAuthVo> {

}
