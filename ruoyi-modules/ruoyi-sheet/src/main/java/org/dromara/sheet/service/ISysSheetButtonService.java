package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.sheet.domain.bo.SysSheetButtonBo;
import org.dromara.sheet.domain.dto.platformList.SearchButtonDto;
import org.dromara.sheet.domain.vo.SysSheetButtonVo;

import java.util.Collection;
import java.util.List;

/**
 * 应用控件Service接口
 *
 * @author Ysl
 * @date 2023-09-01
 */
public interface ISysSheetButtonService {

    /**
     * 查询应用控件
     */
    SysSheetButtonVo queryById(Long sheetFormButtonId);

    /**
     * 查询应用控件列表
     */
    TableDataInfo<SysSheetButtonVo> queryPageList(SysSheetButtonBo bo, PageQuery pageQuery);

    /**
     * 查询应用控件列表
     */
    List<SysSheetButtonVo> queryList(SysSheetButtonBo bo);

    /**
     * 新增应用控件
     */
    Boolean insertByBo(SysSheetButtonBo bo);

    /**
     * 修改应用控件
     */
    Boolean updateByBo(SysSheetButtonBo bo);

    /**
     * 校验并批量删除应用控件信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 修改或保存应用控件
     *
     * @param list
     * @return
     */
    boolean addOrUpdateSheetButton(List<SysSheetButtonVo> list);

}
