package org.dromara.sheet.service;

import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.sheet.domain.bo.SysSheetFormCheckBo;
import org.dromara.sheet.domain.vo.SysSheetFormCheckVo;

import java.util.Collection;
import java.util.List;

/**
 * 单校验Service接口
 *
 * @author Ysl
 * @date 2023-09-16
 */
public interface ISysSheetFormCheckService {

    /**
     * 查询单校验
     */
    SysSheetFormCheckVo queryById(Long sheetFormCheckId);

    /**
     * 查询单校验列表
     */
    TableDataInfo<SysSheetFormCheckVo> queryPageList(SysSheetFormCheckBo bo, PageQuery pageQuery);

    /**
     * 查询单校验列表
     */
    List<SysSheetFormCheckVo> queryList(SysSheetFormCheckBo bo);

    /**
     * 新增单校验
     */
    Boolean insertByBo(SysSheetFormCheckBo bo);

    /**
     * 修改单校验
     */
    Boolean updateByBo(SysSheetFormCheckBo bo);

    /**
     * 校验并批量删除单校验信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
