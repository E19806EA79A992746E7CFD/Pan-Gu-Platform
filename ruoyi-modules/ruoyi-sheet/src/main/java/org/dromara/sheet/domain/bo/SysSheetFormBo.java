package org.dromara.sheet.domain.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import org.dromara.sheet.domain.SysSheetForm;

/**
 * 表单管理业务对象 sys_sheet_form
 *
 * @author lhk
 * @date 2023-09-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysSheetForm.class, reverseConvertGenerate = false)
public class SysSheetFormBo extends BaseEntity {

    /**
     * 主键ID
     */
    @NotNull(message = "主键ID不能为空", groups = { EditGroup.class })
    private Long sheetFormId;

    /**
     * 父级ID
     */
    @NotNull(message = "父级ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long pid;

    /**
     * 表单名称
     */
    private String path;

    /**
     * 表单名称
     */
    @NotBlank(message = "表单名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 显示名称
     */
    private String showName;

    /**
     * 页签
     */
    private String tabName;

    /**
     * 应用ID
     */
    @NotNull(message = "应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sheetId;

    /**
     * 应用名称
     */
    @NotBlank(message = "应用名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String sheetName;

    /**
     * 排序号
     */
    @NotNull(message = "排序号不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sort;

    /**
     * 显示方式
     */
    @NotNull(message = "显示方式不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer showType;

    /**
     * 是否启用
     */
    @NotBlank(message = "是否启用不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 角色ID
     */
    private Long roleId;

}
