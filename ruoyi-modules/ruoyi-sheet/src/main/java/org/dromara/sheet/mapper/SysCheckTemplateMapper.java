package org.dromara.sheet.mapper;

import org.dromara.sheet.domain.SysCheckTemplate;
import org.dromara.sheet.domain.vo.SysCheckTemplateVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 校验模板维护Mapper接口
 *
 * @author zqx
 * @date 2023-09-04
 */
public interface SysCheckTemplateMapper extends BaseMapperPlus<SysCheckTemplate, SysCheckTemplateVo> {

}
