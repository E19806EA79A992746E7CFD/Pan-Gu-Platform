package org.dromara.common.translation.core.impl;

import lombok.AllArgsConstructor;
import org.dromara.common.core.service.SheetService;
import org.dromara.common.translation.annotation.TranslationType;
import org.dromara.common.translation.constant.TransConstant;
import org.dromara.common.translation.core.TranslationInterface;

/**
 * 用户名翻译实现
 *
 * @author Lion Li
 */
@AllArgsConstructor
@TranslationType(type = TransConstant.SHEET_ID_TO_NAME)
public class SheetNameTranslationImpl implements TranslationInterface<String> {

    private final SheetService sheetService;

    @Override
    public String translation(Object key, String other) {
        if (key instanceof Long id) {
            return sheetService.selectSheetNameById(id);
        }
        return null;
    }
}
