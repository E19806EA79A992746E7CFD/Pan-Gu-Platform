package org.dromara.common.core.service;

/**
 * 通用 应用服务
 *
 * @author Ysl
 */
public interface SheetService {

    /**
     * 通过应用ID获取应用名称
     * @param sheetId
     * @return
     */
    String selectSheetNameById(Long sheetId);
}
